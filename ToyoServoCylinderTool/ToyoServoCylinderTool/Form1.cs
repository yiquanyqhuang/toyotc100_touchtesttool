﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Pipes;

namespace ToyoServoCylinderTool
{
    public partial class Form1 : Form
    {
        byte[] send_command;
        int command_lenth; // 不包含 header delimiter  
        FileInfo file;
        SerialPort serialPort = new SerialPort();
        SerialPort serialPort2 = new SerialPort();
        SerialPort serialPort3 = new SerialPort();
        String saveDataFile = null;
        FileStream saveDataFS = null;
        delegate void SetTextCallback(string text);
        delegate void SetTextCallback_WT(string text);
        //建立一個BackgroundWorker執行緒
        int pushcount = 0, timedetect = 0;
        int set = 0, clear = 0, nowpart = 0;
        int Emergency = 0;
        AddScript addScript;

        int MaxindexStride = 0;
        string img_path = System.Environment.CurrentDirectory;
        bool cccsetform_OnOff = false;
        bool cccsetsaveimg = false;

        public Form1()
        {
            InitializeComponent();
            CantStart();
            hScrollBar1.Value = int.Parse(textBox_distance.Text) * 100;
            if (tb_lenInterval != null)
            {
                nUD_curStress.Increment = Convert.ToDecimal(tb_lenInterval.Text);
                trackBar1.LargeChange = (int)(Convert.ToDouble(tb_lenInterval.Text) * 100);
            }
            else
            {
                nUD_curStress.Increment = Convert.ToDecimal(tb_lenInterval.Text);
            }
            trackerbar_time.Start();
            enablewtbutton(false);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            addScript = new AddScript();
            string path = System.IO.Directory.GetCurrentDirectory();
            string filename = string.Concat(path, @"\config.ini");
            addScript.AddObjectToForm(this, filename);


            // create file for "img" saving.
            if (Directory.Exists(img_path + "\\img") == false) Directory.CreateDirectory(path + "\\img");
        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            serialPort.Close();//關閉串口
            serialPort2.Close();//關閉串口
            serialPort3.Close();//關閉串口
        }
        private void CantStart()
        {
            hScrollBar1.Enabled = false;
            textBox_delaytime.Enabled = false;
            textBox_port.Enabled = false;
            textBox_time.Enabled = false;
            textBox_Speed.Enabled = false;
            textBox_enddistance.Enabled = false;
            textBox_startdistance.Enabled = false;
            numericUpDown1.Enabled = false;
            button_SendCommand.Enabled = false;
            button_emergencystop.Enabled = false;
            button_ZeroDistance.Enabled = false;
            button_set.Enabled = false;
            button_autostart.Enabled = false;
            button_Unlock.Enabled = false;
            button_Normaltest.Enabled = false;
            button_calibration.Enabled = false;
            textBox_weight.Enabled = false;
            ToyoOnOff.Enabled = false;
        }
        private void CanStart()
        {
            hScrollBar1.Enabled = true;
            textBox_delaytime.Enabled = true;
            textBox_port.Enabled = true;
            textBox_time.Enabled = true;
            textBox_Speed.Enabled = true;
            textBox_enddistance.Enabled = true;
            textBox_startdistance.Enabled = true;
            numericUpDown1.Enabled = true;
            button_SendCommand.Enabled = true;
            button_emergencystop.Enabled = true;
            button_ZeroDistance.Enabled = true;
            button_Unlock.Enabled = true;
            button_autostart.Enabled = true;
            button_set.Enabled = true;
            button_Normaltest.Enabled = true;
            button_calibration.Enabled = true;
            textBox_weight.Enabled = true;
            ToyoOnOff.Enabled = true;
        }
        private byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            return newByte;
        }
        //byte array to ASCII & combine Hex string  
        //EX  0x32 0x46 -> 2F  
        public static string ConvertHex(String hexString)
        {
            try
            {
                string ascii = string.Empty;
                for (int i = 0; i < hexString.Length; i += 2)
                {
                    String hs = string.Empty;
                    hs = hexString.Substring(i, 2);
                    uint decval = System.Convert.ToUInt32(hs, 16);
                    char character = System.Convert.ToChar(decval);
                    ascii += character;
                }
                return ascii;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            return string.Empty;
        }
        public string ASCIIToHex(string ascii)
        {
            StringBuilder sb = new StringBuilder();
            byte[] inputBytes = Encoding.UTF8.GetBytes(ascii);
            foreach (byte b in inputBytes)
            {
                sb.Append(string.Format("{0:X2} ", b));
            }
            return sb.ToString();
        }
        public string ToyoASCII(string text)
        {
            String[] sp_str = text.Split(new char[] { ' ' });
            command_lenth = sp_str.Length;
            send_command = new byte[command_lenth];

            for (int i = 0; i < send_command.Length; i++)
            {
                //textBox1.Text += "0x" +send_command[i].ToString("X")+ " ";
                send_command[i] = HexToByte(sp_str[i]);
            }

            //convert all data to Hex
            string hex = BitConverter.ToString(send_command).Replace("-",
            string.Empty);
            String result = ConvertHex(hex);
            int sum = 0;
            int index = 0;
            string temp_str = string.Empty;

            for (int i = 0; i < command_lenth / 2; i++)
            {
                temp_str += result[index];
                temp_str += result[++index];
                int value = Convert.ToInt32(temp_str, 16); // 16進位加法
                sum += value;
                ++index;
                temp_str = string.Empty;
            }

            // 2's complement
            sum = ~sum;
            //sum = sum + 1;
            // convert to uint8   將轉換結果存入8bit變數中
            sbyte LRC_origin = (sbyte)sum;
            string hexValue = LRC_origin.ToString("X");

            if (hexValue.Length == 1)
            {
                hexValue = hexValue.Insert(0, "0");
            }
            byte[] LRCArray = null;
            String LRC = string.Empty;
            System.Text.StringBuilder hexNumbers = new System.Text.StringBuilder();
            //Convert to ASCII   LRF store in byteArray
            LRCArray = System.Text.ASCIIEncoding.ASCII.GetBytes(hexValue);
            for (int i = 0; i <= LRCArray.Length - 1; i++)
            {
                LRC += "0x" + LRCArray[i].ToString("x") + " ";
            }

            for (int i = 0; i < LRCArray.Length; i++)
            {
                if (i < LRCArray.Length - 1)
                {
                    //textBox2.Text = LRCArray[i].ToString("X");
                }
                else
                {
                    //textBox3.Text = LRCArray[i].ToString("X");
                }
            }
            int intValue = int.Parse(numericUpDown1.Text);
            string StationValue = intValue.ToString("X");
            string finaltext;
            if (intValue > 15)                                                  //1
            {
                finaltext = "3A " + ASCIIToHex(StationValue) + text + " " + LRCArray[0].ToString("X") + " " + LRCArray[1].ToString("X") + " 0D 0A";
            }
            else
            {
                finaltext = "3A 30 " + ASCIIToHex(StationValue) + text + " " + LRCArray[0].ToString("X") + " " + LRCArray[1].ToString("X") + " 0D 0A";
            }
            return finaltext;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Setfile(textBox_port.Text);
            Loopfile(textBox_port.Text);
        }
        public void Setfile(string port)
        {
            string speed = Convert.ToString(Int32.Parse(textBox_Speed.Text), 16).ToUpper();
            speed = ASCIIToHex(speed);
            string speedcommand, CalZero;
            CalZero = "30 36 32 30 31 45 30 30 30 33";
            if (int.Parse(textBox_Speed.Text) >= 16)
            {
                speedcommand = "30 36 32 30 31 34 30 30 " + speed;
            }
            else
            {
                speedcommand = "30 36 32 30 31 34 30 30 30 " + speed;
            }
            speedcommand = speedcommand.Substring(0, speedcommand.Length - 1);//消除最後一位(" ")
            speedcommand = ToyoASCII(speedcommand);
            CalZero = ToyoASCII(CalZero);
            var path = Directory.GetCurrentDirectory();
            if (Directory.Exists(path + @"\" + @"ToyoTest"))
            {
            }
            else
            {
                Directory.CreateDirectory(path + @"\" + @"ToyoTest");
            }
            path = path + @"\" + @"ToyoTest";
            file = new FileInfo(path + @"\" + "Setfile_0.csv");
            StreamWriter sw = file.CreateText();
            // Add some text to the file.
            sw.WriteLine("Command,>Times >Keyword#,Interval,>COM  >Pin,Function,Sub-function,>SerialPort                   >I/O cmd,AC/USB Switch,Wait,Remark,");
            sw.WriteLine(@"_HEX,,,{0},,,{1},,1000,,", port, CalZero);
            sw.WriteLine(@"_HEX,,,{0},,,{1},,1000,,", port, speedcommand);
            sw.Flush();
            sw.Close();
        }
        public void Loopfile(string port)
        {
            string distance = (Int32.Parse(textBox_distance.Text) * 100).ToString();
            distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
            distance = ASCIIToHex(distance);
            string distancecommand, start, zerodistance;
            start = "30 36 32 30 31 45 30 30 30 31";
            zerodistance = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 30 30";                        //10 2002 0002 04 0000 0000
            int distanceInt = int.Parse(textBox_distance.Text) * 100;
            int delay = int.Parse(textBox_delaytime.Text);
            if (distanceInt >= 4096)
            {
                distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 " + distance;                //10 2002 0002 04 0000 xxxx
            }
            else if (distanceInt < 4096 && distanceInt >= 256)
            {
                distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 " + distance;             //10 2002 0002 04 0000 0xxx
            }
            else
            {
                distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 30 30 ";               //10 2002 0002 04 0000 0xxx
            }
            distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
            distancecommand = ToyoASCII(distancecommand);
            start = ToyoASCII(start);
            zerodistance = ToyoASCII(zerodistance);
            var path = Directory.GetCurrentDirectory();
            if (Directory.Exists(path + @"\" + @"ToyoTest"))
            {
            }
            else
            {
                Directory.CreateDirectory(path + @"\" + @"ToyoTest");
            }
            path = path + @"\" + @"ToyoTest";
            file = new FileInfo(path + @"\" + "Loopfile_1.csv");
            StreamWriter sw = file.CreateText();
            // Add some text to the file.
            sw.WriteLine("Command,>Times >Keyword#,Interval,>COM  >Pin,Function,Sub-function,>SerialPort                   >I/O cmd,AC/USB Switch,Wait,Remark,");
            sw.WriteLine(@"_HEX,,,{0},,,{1},,{2},,", port, distancecommand, delay);
            sw.WriteLine(@"_HEX,,,{0},,,{1},,{2},,", port, start, delay);
            sw.WriteLine(@"_HEX,,,{0},,,{1},,{2},,", port, zerodistance, delay);
            sw.WriteLine(@"_HEX,,,{0},,,{1},,{2},,", port, start, delay);
            sw.Flush();
            sw.Close();
        }
        private void button_buttonOpenCloseCom_Click(object sender, EventArgs e)
        {
            if (!serialPort.IsOpen)//COM若關閉
            {
                try
                {
                    if (comboBox_com.SelectedIndex == -1)
                    {
                        MessageBox.Show("Error: 無效端口,請重新選擇", "Error");
                        return;
                    }
                    string strSerialName = comboBox_com.SelectedItem.ToString();
                    string strBaudRate = "19200";
                    string strDataBit = "8";
                    string strCheckBit = "None";
                    string strStopBit = "1";

                    Int32 iBaudRate = Convert.ToInt32(strBaudRate);
                    Int32 iDataBit = Convert.ToInt32(strDataBit);

                    serialPort.PortName = strSerialName;//COM
                    serialPort.BaudRate = iBaudRate;//波特率
                    serialPort.DataBits = iDataBit;//數據位

                    switch (strStopBit)            //停止位
                    {
                        case "1":
                            serialPort.StopBits = StopBits.One;
                            break;
                        case "1.5":
                            serialPort.StopBits = StopBits.OnePointFive;
                            break;
                        case "2":
                            serialPort.StopBits = StopBits.Two;
                            break;
                        default:
                            MessageBox.Show("Error：停止位參數錯誤!", "Error");
                            break;
                    }
                    switch (strCheckBit)             //校驗位
                    {
                        case "None":
                            serialPort.Parity = Parity.None;
                            break;
                        case "Odd":
                            serialPort.Parity = Parity.Odd;
                            break;
                        case "Even":
                            serialPort.Parity = Parity.Even;
                            break;
                        default:
                            MessageBox.Show("Error：教驗位參數錯誤!", "Error");
                            break;
                    }

                    if (saveDataFile != null)
                    {
                        saveDataFS = File.Create(saveDataFile);
                    }

                    //打開串口
                    serialPort.Open();
                    CanStart();
                    //打開串口後設置變無效
                    comboBox_com.Enabled = false;
                    button_buttonOpenCloseCom.Text = "Close COM";
                    label_COMconnect.Text = "Connect Pass";
                    label_COMconnect.ForeColor = Color.Green;

                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("Error:" + ex.Message, "Error");
                    return;
                }
            }
            else //COM處於打開
            {

                serialPort.Close();//關閉串口
                //串口关闭时设置有效
                comboBox_com.Enabled = true;
                CantStart();
                button_buttonOpenCloseCom.Text = "Open COM";
                label_COMconnect.Text = "No Connect";
                label_COMconnect.ForeColor = Color.Red;
                if (saveDataFS != null)
                {
                    saveDataFS.Close(); // 關閉完建
                    saveDataFS = null;//釋放文件句柄
                }
            }
        }
        private void button_SearchCOM_Click(object sender, EventArgs e)
        {
            comboBox_com.Text = "";
            comboBox_com.Items.Clear();

            string[] str = SerialPort.GetPortNames();
            if (str == null)
            {
                MessageBox.Show("未偵測到COM Port！", "Error");
                return;
            }

            //添加COM
            foreach (string s in str)
            {
                comboBox_com.Items.Add(s);
            }

            //設置預設COM
            comboBox_com.SelectedIndex = 0;
        }
        private void button_SearchCOM2_Click(object sender, EventArgs e)
        {
            comboBox_com2.Text = "";
            comboBox_com2.Items.Clear();

            string[] str = SerialPort.GetPortNames();
            if (str == null)
            {
                MessageBox.Show("未偵測到COM Port！", "Error");
                return;
            }

            //添加COM
            foreach (string s in str)
            {
                comboBox_com2.Items.Add(s);
            }

            //設置預設COM
            comboBox_com2.SelectedIndex = 0;
        }
        private void button_buttonOpenCloseCom2_Click(object sender, EventArgs e)
        {
            if (!serialPort2.IsOpen)//COM若關閉
            {
                try
                {
                    if (comboBox_com2.SelectedIndex == -1)
                    {
                        MessageBox.Show("Error: 無效端口,請重新選擇", "Errors");
                        return;
                    }
                    string strSerialName = comboBox_com2.SelectedItem.ToString();
                    string strBaudRate = "9600";
                    string strDataBit = "8";
                    string strCheckBit = "None";
                    string strStopBit = "1";

                    Int32 iBaudRate = Convert.ToInt32(strBaudRate);
                    Int32 iDataBit = Convert.ToInt32(strDataBit);

                    serialPort2.PortName = strSerialName;//COM
                    serialPort2.BaudRate = iBaudRate;//波特率
                    serialPort2.DataBits = iDataBit;//數據位

                    switch (strStopBit)            //停止位
                    {
                        case "1":
                            serialPort2.StopBits = StopBits.One;
                            break;
                        case "1.5":
                            serialPort2.StopBits = StopBits.OnePointFive;
                            break;
                        case "2":
                            serialPort2.StopBits = StopBits.Two;
                            break;
                        default:
                            MessageBox.Show("Error：停止位參數錯誤!", "Error");
                            break;
                    }
                    switch (strCheckBit)             //校驗位
                    {
                        case "None":
                            serialPort2.Parity = Parity.None;
                            break;
                        case "Odd":
                            serialPort2.Parity = Parity.Odd;
                            break;
                        case "Even":
                            serialPort2.Parity = Parity.Even;
                            break;
                        default:
                            MessageBox.Show("Error：教驗位參數錯誤!", "Error");
                            break;
                    }

                    if (saveDataFile != null)
                    {
                        saveDataFS = File.Create(saveDataFile);
                    }

                    //打開串口
                    serialPort2.Open();
                    //打開串口後設置變無效
                    comboBox_com2.Enabled = false;
                    button_buttonOpenCloseCom2.Text = "Close COM";
                    label_COMconnect2.Text = "Connect Pass";
                    label_COMconnect2.ForeColor = Color.Green;

                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("Error:" + ex.Message, "Error");
                    return;
                }
            }
            else //COM處於打開
            {
                serialPort2.Close();//關閉串口
                //串口关闭时设置有效
                comboBox_com2.Enabled = true;
                button_buttonOpenCloseCom2.Text = "Open COM";
                label_COMconnect2.Text = "No Connect";
                label_COMconnect2.ForeColor = Color.Red;
                if (saveDataFS != null)
                {
                    saveDataFS.Close(); // 關閉完建
                    saveDataFS = null;//釋放文件句柄
                }
            }
           
        }
        private void button_SendCommand_Click(object sender, EventArgs e)
        {
            timedetect = 0;
            timer_TimeDetect.Start();
            if (!serialPort.IsOpen)
            {
                MessageBox.Show("請打開串口", "Error");
                return;
            }
            CantStart();
            button_emergencystop.Enabled = true;
            this.rtxtDataArea.Clear();
            this.label_count.Text = "START";
            this.label_count.ForeColor = Color.Green;
            this.label_count.Text = "Please wait.";
            byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A }; //201E 0003
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            Task.Delay(5000);
            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x30, 0x30, 0x39, 0x39, 0x0D, 0x0A };  //2040 0000
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x31, 0x38, 0x38, 0x31, 0x0D, 0x0A };  //2040 0018
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            serialPort2.DataReceived += SerialPortDataReceived;
        }
        string gettext = "";
        int loop_counter = 1;
        bool postarget = false;
        bool nagtarget = false;
        bool re_postarget = false;
        double currentforceMax = 0.0;
        double currentforceMin = 0.0;
        double currentforce = 0.0;
        double CorrectionValue = 0.0;
        int forcenum = 2;
        byte[] bytesToSendstride;
        byte[] readbytesstride;
        bool stopprogram = false;
        int voltage_1 = 0;
        int voltage_2 = 0;
        int voltage_3 = 0;
        string voltagemark;
        string buffer = null;
        string totxtstride = null;
        private void SetText(string text)
        {
            byte[] bytesToSend;
            CorrectionValue = Convert.ToDouble(tb_CorrectionValue.Text);
            //invokeRequired required compares the thread ID of the calling thread to the thread of the creating thread.
            // if these threads are different, it returns true
            //part0 => 下降偵測重量 1=>持續按壓  2>持續感測(校準用) 3>固定距離按壓 4> script 運作
            if (Emergency == 0)
            {

                if (this.rtxtDataArea.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(SetText);
                    this.Invoke(d, new object[] { text });

                }
                else
                {
                    this.rtxtDataArea.Focus(); //让文本框获取焦点 
                    this.rtxtDataArea.Select(this.rtxtDataArea.TextLength, 0);//设置光标的位置到文本尾
                    this.rtxtDataArea.ScrollToCaret();//滚动到控件光标处 
                    this.rtxtDataArea.AppendText(text);
                    if (nowpart == 1)
                    {
                        if (set == 0)
                        {
                            setting();
                            serialPort2.Write("start");
                            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x30, 0x30, 0x39, 0x39, 0x0D, 0x0A };
                            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x31, 0x38, 0x38, 0x31, 0x0D, 0x0A };
                            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                            Thread.Sleep(1000);
                            set = 1;
                        }
                        else
                        {
                            if (Regex.Split(rtxtDataArea.Text, "\n").Length != 0 && clear == 0)
                            {
                                this.rtxtDataArea.Rtf = "";
                                clear = 1;
                            }
                            pushcount = Regex.Split(rtxtDataArea.Text, "\n").Length;
                            if (pushcount > 1)
                            {
                                timer_TimeDetect.Stop();
                            }
                            this.label_count.Text = "NowCount:" + "\r\n" + pushcount.ToString();

                            if (pushcount > int.Parse(textBox_time.Text))
                            {
                                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };
                                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                                timer_TimeDetect.Stop();
                                CanStart();
                                clear = 0;
                                nowpart = 4;
                                set = 0;
                                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A };
                                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                                this.label_count.Text = "STOP";
                                this.label_count.ForeColor = Color.Red;
                            }
                        }
                    }
                    else if (nowpart == 0)
                    {
                        //string gettext;
                        hScrollBar1.Value++;
                        double a = Math.Round((double)hScrollBar1.Value / 100, 2);
                        textBox_distance.Text = a.ToString();
                        Thread.Sleep(300);
                        gettext = GetSpecialLines(Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1, Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1);

                        if (gettext != "")
                        {
                            if (float.Parse(gettext) >= float.Parse(this.textBox_weight.Text) && float.Parse(gettext) <= float.Parse(this.textBox_weight.Text) * 1.1)
                            {
                                label8.Text = "Weight:" + gettext.ToString();
                                this.label8.ForeColor = Color.Green;
                                nowpart = 1;
                                set = 0;
                            }
                            else if (float.Parse(gettext) > float.Parse(this.textBox_weight.Text) * 1.1)
                            {
                                serialPort2.Write("stop");
                                MessageBox.Show("感測壓力超出輸入值10%", "Error");
                                this.label_count.Text = "STOP";
                                this.label_count.ForeColor = Color.Red;
                                CanStart();
                                button_autostart.Enabled = false;
                                button_ZeroDistance.Enabled = false;
                                button_emergencystop.Enabled = false;
                                hScrollBar1.Enabled = false;
                                clear = 0;
                                nowpart = 4;
                                set = 0;
                                Emergency = 1;
                            }
                        }
                        label_count.Text = "Weight:" + gettext.ToString();
                        Task.Delay(100);
                    }

                    else if (nowpart == 3)
                    {
                        if (set == 0)
                        {
                            settingstart_normal();
                            serialPort2.Write("start");
                            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x30, 0x30, 0x39, 0x39, 0x0D, 0x0A };
                            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x31, 0x38, 0x38, 0x31, 0x0D, 0x0A };
                            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                            Thread.Sleep(1000);
                            set = 1;
                        }
                        else
                        {
                            if (Regex.Split(rtxtDataArea.Text, "\n").Length != 0 && clear == 0)
                            {
                                this.rtxtDataArea.Rtf = "";
                                clear = 1;
                            }
                            pushcount = Regex.Split(rtxtDataArea.Text, "\n").Length;

                            if (pushcount > 1)
                            {
                                timer_TimeDetect.Stop();
                            }
                            this.label_count.Text = "NowCount:" + "\r\n" + pushcount.ToString();
                            if (pushcount > int.Parse(textBox_time.Text))
                            {
                                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };
                                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                                timer_TimeDetect.Stop();
                                CanStart();
                                clear = 0;
                                nowpart = 4;
                                set = 0;
                                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A };
                                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                                this.label_count.Text = "STOP";
                                this.label_count.ForeColor = Color.Red;
                                this.label8.Text = "";
                            }
                        }
                    }
                    else if (nowpart == 2)
                    {
                        //string gettext;
                        gettext = GetSpecialLines(Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1, Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1);
                        label8.Text = "Weight:" + gettext.ToString();
                        Task.Delay(100);

                    }
                    else if (nowpart == 4)
                    {

                        gettext = GetSpecialLines(Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1, Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1);
                        label8.Text = "Weight:" + gettext.ToString();
                        currentforce = Convert.ToDouble(gettext);

                        ///------


                        if (currentforce > currentforceMax)
                            currentforceMax = currentforce;

                        if (Recordtype_no.Checked)
                        {
                            currentforceMax = 0;
                        }

                        if (set == 0)
                        {

                            runprogram();
                            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x30, 0x30, 0x39, 0x39, 0x0D, 0x0A };
                            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                            bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x31, 0x38, 0x38, 0x31, 0x0D, 0x0A };
                            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                            //Task.Delay(300);
                            //Stride_timer.Start();
                            set = 1;

                        }
                        else
                        {
                            //toyo下至上(or上至下)為一個週期
                            if (postarget == false & currentforce > CorrectionValue)
                                postarget = true;

                            if (nagtarget == false & currentforce < 0.00)
                                nagtarget = true;
                            ///------
                            ///------
                            string timedata = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                            ////3A 30 31 30 33 31 30 30 45 30 30 30 31 44 44 0D 0A
                            bytesToSendstride = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x33, 0x31, 0x30, 0x30, 0x45, 0x30, 0x30, 0x30, 0x31, 0x44, 0x44, 0x0D, 0x0A };
                            serialPort.Write(bytesToSendstride, 0, bytesToSendstride.Length);
                            Task.Delay(80);
                            //Thread.Sleep(30);
                            int len = serialPort.BytesToRead;
                            readbytesstride = new byte[len];
                            serialPort.Read(readbytesstride, 0, len);
                            //listBox1.Items.Add(Encoding.UTF8.GetString(readbytesstride));
                            //listBox1.SelectedIndex = listBox1.Items.Count - 1;
                            if (len == 30)
                            {
                                LoopBuffer = Encoding.UTF8.GetString(readbytesstride);     //byte trans to ascii

                                if (LoopBuffer != "" & LoopBuffer != null)
                                {
                                    str = LoopBuffer.Substring(9, 2);
                                    label12.Text = str;
                                }
                                try
                                {
                                    strnum = Convert.ToInt32(str, 16);
                                }
                                catch
                                {
                                    MessageBox.Show("有干擾");
                                    return;
                                }
                                string pushcount_stride = string.Concat(loop_counter.ToString(), loopround.ToString());


                                if (INTindex != strnum & current_stride != "11") //起始都是一
                                {
                                    INTindex = strnum;
                                    if (current_stride != pushcount_stride)
                                    {
                                        dgvTables.Rows.Add(timedata, loop_counter.ToString(), loopround.ToString(), currentforceMax.ToString(), voltagemark);
                                        //currentforceMax = 0;
                                        totxtstride = string.Concat(loop_counter.ToString(), loopround.ToString().PadLeft(3, '0'));
                                        SimpleWriteAsync(totxtstride);
                                        current_stride = pushcount_stride;
                                        voltagemark = null;
                                        this.dgvTables.FirstDisplayedScrollingRowIndex = this.dgvTables.Rows.Count - 1;

                                    }
                                    loopround++;
                                }
                                else if (INTindex != strnum & current_stride == "11")
                                {
                                    INTindex = strnum;

                                    dgvTables.Rows.Add(timedata, loop_counter.ToString(), loopround.ToString(), currentforceMax.ToString(), voltagemark);
                                    currentforceMax = 0;
                                    totxtstride = string.Concat(loop_counter.ToString(), loopround.ToString().PadLeft(3, '0'));
                                    SimpleWriteAsync(totxtstride);
                                    current_stride = pushcount_stride;
                                    loopround++;
                                    voltagemark = null;
                                    this.dgvTables.FirstDisplayedScrollingRowIndex = this.dgvTables.Rows.Count - 1;

                                }
                                if (loopround == MaxindexStride + 1)
                                {

                                    loopround = 1;
                                    loop_counter++;

                                }


                                pushcount = loop_counter;


                            }

                            //listBox1.Items.Add( current_stride + "~" + len.ToString().PadLeft(2,'0') + "~" + gettext.ToString());
                            //listBox1.SelectedIndex = listBox1.Items.Count - 1;

                            ///------
                            ///------                           

                            if (postarget & nagtarget)
                            {
                                label14.Text = (forcenum).ToString();
                                forcenum++;
                                postarget = false;
                                nagtarget = false;
                                currentforceMax = 0;
                            }

                            if (loop_counter == int.Parse(textBox_time.Text) + 1 & loopround == 1)
                            {
                                stopprogram = true;
                                //listBox1.Items.Add(loop_counter.ToString() + "~" + loopround.ToString());
                                //listBox1.SelectedIndex = listBox1.Items.Count - 1;
                                totxtstride = string.Concat(loop_counter.ToString(), loopround.ToString().PadLeft(3, '0'));
                                SimpleWriteAsync(totxtstride);
                            }


                            //int insresduals = loop_counter % 2;
                            if (stopprogram)//(pushcount == int.Parse(textBox_time.Text) & loopround == MaxindexStride )
                            {
                                Thread.Sleep(500);
                                //MessageBox.Show("STOP");
                                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };  //201e 0008
                                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                                timer_TimeDetect.Stop();
                                CanStart();
                                clear = 0;
                                nowpart = 2;
                                set = 0;
                                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A };  //201e 0003
                                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                                this.label_count.Text = "STOP";
                                this.label_count.ForeColor = Color.Red;
                                this.label8.Text = "";

                                //Stride_timer.Stop();
                            }

                        }

                    }
                    else if (nowpart == 6)
                    {
                        ;
                    }
             

                }

            }

        }

        /// <summary>
        /// 這是給跨執行程式互相傳送資料用
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="Msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, UIntPtr
        wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hwnd, uint wMsg, int wParam, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern uint RegisterWindowMessage(string lpString);
        uint MSG_SHOW = RegisterWindowMessage("Show Message");
        [DllImport("user32.dll")]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);


        string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);//桌面路徑 
        public async Task SimpleWriteAsync(string loopstr)
        {
            IntPtr nPrt = FindWindow(null, "SerialPortTable");//找TestB的IntPtr 用來代表指標或控制代碼
            if (nPrt != IntPtr.Zero && loopstr.Trim() != "")
            {
                try
                {
                    //MessageBox.Show((string)loopstr.Trim().ToString());
                    string tempWord = loopstr.Trim();
                    int iNum = int.Parse(tempWord);//把tempWord字串 轉為int,所以值不是數字會變例外
                    SendMessage(nPrt, MSG_SHOW, iNum, IntPtr.Zero);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }





        private void bt_CleardgvTables_Click(object sender, EventArgs e)
        {
            loop_counter = 1;
            dgvTables.Rows.Clear();
            //currentforceMax = 0.0;
            pushcount = 0;
            forcenum = 0;
            loopround = 1;
            stopprogram = false;
            strnum = 1;
            INTindex = 1;
            current_stride = "11";
            LoopBuffer = "X";
            //label12.Text = "0";
            string stoprecord = string.Concat("9", "129");
            SimpleWriteAsync(stoprecord);
            listBox1.Items.Clear();
            current_strides = new string[2] { "1", "1" };
        }
        string LoopBuffer = null;
        int INTindex = 1;
        int loopround = 1;
        string[] MAXARRAY;
        string str = null;
        int strnum = 1;
        string current_stride = "11";
        string[] current_strides = new string[2] { "1", "4" };
        private void Stride_timer_Tick(object sender, EventArgs e)
        {
            ////3A 30 31 30 33 31 30 30 45 30 30 30 31 44 44 0D 0A
            byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x33, 0x31, 0x30, 0x30, 0x45, 0x30, 0x30, 0x30, 0x31, 0x44, 0x44, 0x0D, 0x0A };
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            string timedata = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Thread.Sleep(30);
            int len = serialPort.BytesToRead;
            byte[] bytes = new byte[len];
            serialPort.Read(bytes, 0, len);
            if (false) //(len == 15)
            {
                LoopBuffer = Encoding.UTF8.GetString(bytes);     //byte trans to ascii

                if (LoopBuffer != "" & LoopBuffer != null)
                {
                    str = LoopBuffer.Substring(9, 2);
                    label12.Text = str;
                }
                try
                {
                    strnum = Convert.ToInt32(str, 16);
                }
                catch
                {
                    MessageBox.Show("有干擾");
                    return;
                }
                string pushcount_stride = string.Concat(loop_counter.ToString(), loopround.ToString());


                if (INTindex != strnum & current_stride != "11") //起始都是一
                {
                    INTindex = strnum;
                    if (current_stride != pushcount_stride)
                    {
                        dgvTables.Rows.Add(timedata, loop_counter.ToString(), loopround.ToString(), currentforceMax.ToString());
                        currentforceMax = 0;
                        current_stride = pushcount_stride;
                    }
                    loopround++;
                }
                else if (INTindex != strnum & current_stride == "11")
                {
                    INTindex = strnum;

                    dgvTables.Rows.Add(timedata, loop_counter.ToString(), loopround.ToString(), currentforceMax.ToString());
                    //currentforceMax = 0;
                    current_stride = pushcount_stride;

                    loopround++;
                }




                if (loopround == MaxindexStride + 1)
                {
                    loopround = 1;
                    loop_counter++;
                }

                pushcount = loop_counter;
            }


        }
        private void bt_ForceQuit_Click(object sender, EventArgs e)
        {
            byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };  //201e 0008
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            serialPort2.Write("stop");
            this.label_count.Text = "STOP";
            this.label_count.ForeColor = Color.Red;

            CanStart();
            clear = 0;
            nowpart = 2;
            set = 0;
            Emergency = 1;
            Task.Delay(1000);
            Emergency = 0;
        }

        private void SerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var serialPort = (SerialPort)sender;
            var data = serialPort.ReadExisting();
            SetText(data);
        }
        private void button_emergencystop_Click(object sender, EventArgs e)
        {
            RunScheduler_timer.Stop();
            byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };  // 2015 0008
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            //serialPort2.Write("stop");
            this.label_count.Text = "STOP";
            this.label_count.ForeColor = Color.Red;
            CanStart();
            button_autostart.Enabled = false;
            button_ZeroDistance.Enabled = false;
            button_emergencystop.Enabled = false;
            hScrollBar1.Enabled = false;
            button_Normaltest.Enabled = false;
            button_calibration.Enabled = false;
            clear = 0;
            nowpart = 2;
            set = 0;
            Emergency = 1;
            Task.Delay(1000);
            Emergency = 0;
            button_autostart.Enabled = true;
            button_ZeroDistance.Enabled = true;
            button_emergencystop.Enabled = true;
            hScrollBar1.Enabled = true;
            button_Normaltest.Enabled = true;
            button_calibration.Enabled = true;
            Stride_timer.Stop();
            trace_report_time.Stop();
           
        }
        private void button_Unlock_Click(object sender, EventArgs e)
        {
            Emergency = 0;
            button_autostart.Enabled = true;
            button_ZeroDistance.Enabled = true;
            button_emergencystop.Enabled = true;
            hScrollBar1.Enabled = true;
            button_Normaltest.Enabled = true;
            button_calibration.Enabled = true;
        }
        private void button_set_Click(object sender, EventArgs e)
        {
            //if (!serialPort.IsOpen)
            //{
            //    MessageBox.Show("請打開串口", "Error");
            //    return;
            //}
            ////setting();
            //MessageBox.Show("Setting OK");
        }
        private void settingstart()
        {
            //delay(起始)
            string delaytime = Convert.ToString(Int32.Parse(textBox_delaytime.Text) / 2, 16).ToUpper();
            delaytime = ASCIIToHex(delaytime);
            string delaytimecommand;
            if (int.Parse(textBox_delaytime.Text) >= 4096)
            {
                delaytimecommand = "30 36 39 30 31 43 " + delaytime;
            }
            else if (int.Parse(textBox_delaytime.Text) < 4096 && int.Parse(textBox_delaytime.Text) >= 256)
            {
                delaytimecommand = "30 36 39 30 31 43 30 " + delaytime;
            }
            else if (int.Parse(textBox_delaytime.Text) < 256 && int.Parse(textBox_delaytime.Text) >= 16)
            {
                delaytimecommand = "30 36 39 30 31 43 30 30 " + delaytime;
            }
            else
            {
                delaytimecommand = "30 36 39 30 31 43 30 30 30 " + delaytime;
            }

            delaytimecommand = delaytimecommand.Substring(0, delaytimecommand.Length - 1);//消除最後一位(" ")
            delaytimecommand = ToyoASCII(delaytimecommand);
            //開始距離
            string distance = (double.Parse(textBox_distance.Text) * 100).ToString();
            distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
            distance = ASCIIToHex(distance);
            string distancecommand;
            double distanceIntdouble = double.Parse(textBox_distance.Text) * 100;
            int distanceInt = (int)distanceIntdouble;

            if (distanceInt >= 4096)
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 " + distance;
            }
            else if (distanceInt < 4096 && distanceInt >= 256)
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 30 " + distance;
            }
            else if (distanceInt < 256 && distanceInt >= 16)
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 30 30 " + distance;
            }
            else
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance;
            }
            distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
            distancecommand = ToyoASCII(distancecommand);
            byte[] bytedistance = distancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedistance, 0, bytedistance.Length);
            Task.Delay(500);
            byte[] bytedelay = delaytimecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedelay, 0, bytedelay.Length);
        }

        private void setting()
        {
            //以下為delay
            string delaytime = Convert.ToString(Int32.Parse(textBox_delaytime.Text) / 2, 16).ToUpper();
            delaytime = ASCIIToHex(delaytime);
            string delaytimecommand;
            if (int.Parse(textBox_delaytime.Text) >= 4096)
            {
                delaytimecommand = "30 36 39 30 32 43 " + delaytime;                                // 06 90 2cxx
            }
            else if (int.Parse(textBox_delaytime.Text) < 4096 && int.Parse(textBox_delaytime.Text) >= 256)
            {
                delaytimecommand = "30 36 39 30 32 43 30 " + delaytime;                             // 06 90 2c0x
            }
            else if (int.Parse(textBox_delaytime.Text) < 256 && int.Parse(textBox_delaytime.Text) >= 16)
            {
                delaytimecommand = "30 36 39 30 32 43 30 30 " + delaytime;                          // 06 90 2c00 xxxx
            }
            else
            {
                delaytimecommand = "30 36 39 30 32 43 30 30 30 " + delaytime;
            }
            delaytimecommand = delaytimecommand.Substring(0, delaytimecommand.Length - 1);//消除最後一位(" ")
            delaytimecommand = ToyoASCII(delaytimecommand);
            //以下為速度
            string speed = Convert.ToString(Int32.Parse(textBox_Speed.Text), 16).ToUpper();
            speed = ASCIIToHex(speed);
            string speedcommand;
            if (int.Parse(textBox_Speed.Text) >= 16)
            {
                speedcommand = "30 36 39 30 32 33 30 30 " + speed;
            }
            else
            {
                speedcommand = "30 36 39 30 32 33 30 30 30 " + speed;
            }
            speedcommand = speedcommand.Substring(0, speedcommand.Length - 1);//消除最後一位(" ")
            speedcommand = ToyoASCII(speedcommand);
            //以下為結束距離
            string distance = (double.Parse(textBox_distance.Text) * 100).ToString();
            distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
            distance = ASCIIToHex(distance);
            string distancecommand;
            double distanceIntdouble = double.Parse(textBox_distance.Text) * 100;
            int distanceInt = (int)distanceIntdouble;
            //int delay = int.Parse(textBox_delaytime.Text);
            if (distanceInt >= 4096)
            {
                distancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 " + distance;            // 10 9021 0002 04 0000 xxxx
            }
            else if (distanceInt < 4096 && distanceInt >= 256)
            {
                distancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 30 " + distance;         // 10 9021 0002 04 0000 0xxx
            }
            else if (distanceInt < 256 && distanceInt >= 16)
            {
                distancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 30 30 " + distance;      // 10 9021 0002 04 0000 00xx
            }
            else
            {
                distancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance;   // 10 9021 0002 04 0000 000x
            }
            distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
            distancecommand = ToyoASCII(distancecommand);

            byte[] bytedistance = distancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedistance, 0, bytedistance.Length);
            Task.Delay(500);
            byte[] bytespeed = speedcommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytespeed, 0, bytespeed.Length);
            Task.Delay(500);
            byte[] bytedelay = delaytimecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedelay, 0, bytedelay.Length);

            //byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };
            //serialPort.Write(bytesToSend, 0, bytesToSend.Length);
        }
        private void settingstart_normal()
        {
            //delay(起始)
            string delaytime = Convert.ToString(Int32.Parse(textBox_delaytime.Text) / 2, 16).ToUpper();
            delaytime = ASCIIToHex(delaytime);
            string delaytimecommand;
            if (int.Parse(textBox_delaytime.Text) >= 4096)
            {
                delaytimecommand = "30 36 39 30 31 43 " + delaytime;
            }
            else if (int.Parse(textBox_delaytime.Text) < 4096 && int.Parse(textBox_delaytime.Text) >= 256)
            {
                delaytimecommand = "30 36 39 30 31 43 30 " + delaytime;
            }
            else if (int.Parse(textBox_delaytime.Text) < 256 && int.Parse(textBox_delaytime.Text) >= 16)
            {
                delaytimecommand = "30 36 39 30 31 43 30 30 " + delaytime;
            }
            else
            {
                delaytimecommand = "30 36 39 30 31 43 30 30 30 " + delaytime;
            }
            delaytimecommand = delaytimecommand.Substring(0, delaytimecommand.Length - 1);//消除最後一位(" ")
            delaytimecommand = ToyoASCII(delaytimecommand);
            //開始距離
            string distance = (double.Parse(textBox_startdistance.Text) * 100).ToString();
            distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
            distance = ASCIIToHex(distance);
            string distancecommand;
            double distancedouble = double.Parse(textBox_startdistance.Text) * 100;
            int distanceInt = (int)distancedouble;
            if (distanceInt >= 4096)
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 " + distance;
            }
            else if (distanceInt < 4096 && distanceInt >= 256)
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 30 " + distance;
            }
            else if (distanceInt < 256 && distanceInt >= 16)
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 30 30 " + distance;
            }
            else
            {
                distancecommand = "31 30 39 30 31 31 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance;
            }
            distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
            distancecommand = ToyoASCII(distancecommand);
            //以下為delay
            string delaytime2 = Convert.ToString(Int32.Parse(textBox_delaytime.Text) / 2, 16).ToUpper();
            delaytime2 = ASCIIToHex(delaytime2);
            string delaytimecommand2;
            if (int.Parse(textBox_delaytime.Text) >= 4096)
            {
                delaytimecommand2 = "30 36 39 30 32 43 " + delaytime2;
            }
            else if (int.Parse(textBox_delaytime.Text) < 4096 && int.Parse(textBox_delaytime.Text) >= 256)
            {
                delaytimecommand2 = "30 36 39 30 32 43 30 " + delaytime2;
            }
            else if (int.Parse(textBox_delaytime.Text) < 256 && int.Parse(textBox_delaytime.Text) >= 16)
            {
                delaytimecommand2 = "30 36 39 30 32 43 30 30 " + delaytime2;
            }
            else
            {
                delaytimecommand2 = "30 36 39 30 32 43 30 30 30 " + delaytime2;
            }
            delaytimecommand2 = delaytimecommand2.Substring(0, delaytimecommand2.Length - 1);//消除最後一位(" ")
            delaytimecommand2 = ToyoASCII(delaytimecommand2);
            //以下為速度
            string speed = Convert.ToString(Int32.Parse(textBox_Speed.Text), 16).ToUpper();
            speed = ASCIIToHex(speed);
            string speedcommand;
            if (int.Parse(textBox_Speed.Text) >= 16)
            {
                speedcommand = "30 36 39 30 32 33 30 30 " + speed;
            }
            else
            {
                speedcommand = "30 36 39 30 32 33 30 30 30 " + speed;
            }
            speedcommand = speedcommand.Substring(0, speedcommand.Length - 1);//消除最後一位(" ")
            speedcommand = ToyoASCII(speedcommand);
            //以下為結束距離
            string enddistance = (double.Parse(textBox_enddistance.Text) * 100).ToString();
            enddistance = Convert.ToString(Int32.Parse(enddistance), 16).ToUpper();
            enddistance = ASCIIToHex(enddistance);
            string enddistancecommand;
            double enddistanceIntdouble = double.Parse(textBox_enddistance.Text) * 100;
            int enddistanceInt = (int)enddistanceIntdouble;
            //int delay = int.Parse(textBox_delaytime.Text);
            if (enddistanceInt >= 4096)
            {
                enddistancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 " + enddistance;
            }
            else if (enddistanceInt < 4096 && enddistanceInt >= 256)
            {
                enddistancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 30 " + enddistance;
            }
            else if (enddistanceInt < 256 && enddistanceInt >= 16)
            {
                enddistancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 30 30 " + enddistance;
            }
            else
            {
                enddistancecommand = "31 30 39 30 32 31 30 30 30 32 30 34 30 30 30 30 30 30 30 " + enddistance;
            }
            enddistancecommand = enddistancecommand.Substring(0, enddistancecommand.Length - 1);//消除最後一位(" ")
            enddistancecommand = ToyoASCII(enddistancecommand);

            byte[] bytedistance = distancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedistance, 0, bytedistance.Length);
            Task.Delay(500);
            byte[] bytedistanceend = enddistancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedistanceend, 0, bytedistanceend.Length);
            Task.Delay(500);
            byte[] bytespeed = speedcommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytespeed, 0, bytespeed.Length);
            Task.Delay(500);
            byte[] bytedelay2 = delaytimecommand2.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedelay2, 0, bytedelay2.Length);
            Task.Delay(500);
            byte[] bytedelay = delaytimecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedelay, 0, bytedelay.Length);
        }
        private void button_ZeroDistance_Click(object sender, EventArgs e)
        {
            if (!serialPort.IsOpen)
            {
                MessageBox.Show("請打開串口", "Error");
                return;
            }
            byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A };   //201E 0003
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            hScrollBar1.Value = 0;
            textBox_distance.Text = "0";
            Thread.Sleep(3000);
            MessageBox.Show("CalZero OK.", "CalZero");
        }
        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            hScrollBar1.ValueChanged += new EventHandler(this.hScrollBar1_ValueChanged);
        }


        private void hScrollBar1_ValueChanged(Object sender, EventArgs e)
        {
            double a = Math.Round(((double)hScrollBar1.Value) / 100, 2);
            textBox_distance.Text = a.ToString();
        }
        private void textBox_distance_TextChanged(object sender, EventArgs e)
        {

            string distance = (double.Parse(textBox_distance.Text) * 100).ToString();       //Max 50
            distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
            distance = ASCIIToHex(distance);
            string distancecommand;
            double distanceInt = double.Parse(textBox_distance.Text) * 100;

            int delay = int.Parse(textBox_delaytime.Text);

            if (distanceInt >= 4096)
            {
                distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 " + distance;            //連續讀寫 10 2002 0002 04 0000 xxxx
            }
            else if (distanceInt < 4096 && distanceInt >= 256)
            {
                distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 " + distance;         //連續讀寫 10 2002 0002 04 0000 0xxx
            }
            else if (distanceInt < 256 && distanceInt >= 16)
            {
                distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 " + distance;      //連續讀寫 10 2002 0002 04 0000 00xx
            }
            else
            {
                distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance;   //連續讀寫 10 2002 0002 04 0000 000x
            }
            distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
            distancecommand = ToyoASCII(distancecommand);
            byte[] start = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x31, 0x42, 0x41, 0x0D, 0x0A };     // 06 201E 0001 #ABS絕對位置移動
            serialPort.Write(start, 0, start.Length);
            Thread.Sleep(100);
            byte[] bytedistance = distancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();     //00 01 02 03 ->0x0 0x1
            serialPort.Write(bytedistance, 0, bytedistance.Length);
            Thread.Sleep(100);

        }
        private void timer_TimeDetect_Tick(object sender, EventArgs e)
        {
            timedetect++;
            if (timedetect > 10)
            {
                timer_TimeDetect.Stop();
                byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };   //201E 0008
                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                MessageBox.Show("No pressure value detected.");
                this.label_count.Text = "STOP";
                this.label_count.ForeColor = Color.Red;
                CanStart();
            }
        }

        private void button_autostart_Click(object sender, EventArgs e)
        {
            timedetect = 0;
            //timer_TimeDetect.Start();
            if (!serialPort.IsOpen)
            {
                MessageBox.Show("請打開串口", "Error");
                return;
            }
            if (!serialPort2.IsOpen)
            {
                MessageBox.Show("請打開串口", "Error");
                return;
            }
            CantStart();
            clear = 0;
            nowpart = 0; //part0 => 下降偵測重量 1=>持續按壓  2=>持續感測(校準用) 3=>固定距離按壓
            set = 0;
            button_emergencystop.Enabled = true;
            this.rtxtDataArea.Rtf = "";
            serialPort2.Write("stop");
            this.label_count.Text = "START";
            this.label_count.ForeColor = Color.Green;
            this.label_count.Text = "Please wait.";
            this.label8.Text = "Please wait.";
            Thread.Sleep(1000);
            settingstart();
            serialPort2.DataReceived += SerialPortDataReceived;
            
        }

        private void button_calibration_Click(object sender, EventArgs e)
        {
            nowpart = 2;
            serialPort2.Write("stop");
            serialPort2.DataReceived += SerialPortDataReceived;
        }
        private void button_Normaltest_Click(object sender, EventArgs e)
        {
            timedetect = 0;
            timer_TimeDetect.Start();
            if (!serialPort.IsOpen)
            {
                MessageBox.Show("請打開串口", "Error");
                return;
            }
            if (!serialPort2.IsOpen)
            {
                MessageBox.Show("請打開串口", "Error");
                return;
            }
            CantStart();
            clear = 0;
            nowpart = 3; //part0 => 下降偵測重量 1=>持續按壓  2>持續感測(校準用) 3>固定距離按壓
            set = 0;
            button_emergencystop.Enabled = true;
            this.rtxtDataArea.Rtf = "";
            serialPort2.Write("stop");
            this.label_count.Text = "START";
            this.label_count.ForeColor = Color.Green;
            this.label_count.Text = "Please wait.";
            this.label8.Text = "Please wait.";
            Thread.Sleep(1000);
            serialPort2.DataReceived += SerialPortDataReceived;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String texture = "";
            texture = textBox1.Text;
            textBox2.Text = "";
            textBox2.Text = ToyoASCII(texture);//只能計算含功能碼以後的符號
            string sub_word = "";
            //sub_word = Convert.ToString(serialPort.ReadByte(), 16).ToUpper().PadLeft(2, '0');//.PadRight(3, ' ');                   
            //listBox1.Items.Add(sub_word);
            //listBox1.SelectedIndex = listBox1.Items.Count - 1;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string strmsg = textBox2.Text;
            byte[] bytedistance = strmsg.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(bytedistance, 0, bytedistance.Length);
            Thread.Sleep(100);

            //string sub_word = "";
            //sub_word = Convert.ToString(serialPort.ReadByte(), 16).ToUpper().PadLeft(2, '0');//.PadRight(3, ' ');                   
            //listBox1.Items.Add(sub_word);
            //listBox1.SelectedIndex = listBox1.Items.Count - 1;

            string buffer = null;
            int len = serialPort.BytesToRead;
            byte[] bytes = new byte[len];
            serialPort.Read(bytes, 0, len);


            //buffer = byteToHexStr(bytes);                               //byte trans to hex

            buffer = System.Text.Encoding.Default.GetString(bytes);     //byte trans to ascii
            listBox1.Items.Add(buffer);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;

        }
        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {

                Parallel.ForEach(bytes, (d) =>
                {
                    //returnStr = Convert.ToString(bytes[i], 16);
                    returnStr += d.ToString("X2");
                    returnStr += d.ToString(" ");
                });
                /*
                for (int i = 0; i < bytes.Length; i++)
                {
                    //returnStr = Convert.ToString(bytes[i], 16);
                    returnStr += bytes[i].ToString("X2");
                    returnStr += bytes[i].ToString(" ");
                }*/
            }
            return returnStr.Trim();
        }
        string contents = null;
        private void RunScheduler_timer_Tick(object sender, EventArgs e)
        {
            if (set == 0)
            {
                runprogram();
                byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x30, 0x30, 0x39, 0x39, 0x0D, 0x0A };       //2040 0000 99
                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x34, 0x30, 0x30, 0x30, 0x31, 0x38, 0x38, 0x31, 0x0D, 0x0A };              //2040 0018 81
                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                set = 1;

            }
            else
            {

                string timedata = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ///
                ///3A 30 31 30 33 31 30 30 45 30 30 30 31 44 44 0D 0A
                bytesToSendstride = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x33, 0x31, 0x30, 0x30, 0x45, 0x30, 0x30, 0x30, 0x31, 0x44, 0x44, 0x0D, 0x0A };        //100E 0001 DD
                serialPort.Write(bytesToSendstride, 0, bytesToSendstride.Length);
                Task.Delay(80);                //Thread.Sleep(30);
                int len = serialPort.BytesToRead;
                readbytesstride = new byte[len];
                serialPort.Read(readbytesstride, 0, len);

                if (len == 15)
                {
                    LoopBuffer = Encoding.UTF8.GetString(readbytesstride);     //byte trans to ascii

                    if (LoopBuffer != "" & LoopBuffer != null)
                    {
                        str = LoopBuffer.Substring(9, 2);
                        label12.Text = str;
                    }
                    try
                    {
                        strnum = Convert.ToInt32(str, 16);
                    }
                    catch
                    {
                        MessageBox.Show("有干擾");
                        return;
                    }
                    string pushcount_stride = string.Concat(loop_counter.ToString(), loopround.ToString());


                    if (INTindex != strnum & current_stride != "11") //起始都是一
                    {
                        INTindex = strnum;
                        if (current_stride != pushcount_stride)
                        {
                            dgvTables.Rows.Add(timedata, loop_counter.ToString(), loopround.ToString(), currentforceMax.ToString(), voltagemark);
                            //currentforceMax = 0;
                            totxtstride = string.Concat(loop_counter.ToString(), loopround.ToString().PadLeft(3, '0'));
                            SimpleWriteAsync(totxtstride);
                            current_stride = pushcount_stride;
                            voltagemark = null;
                            this.dgvTables.FirstDisplayedScrollingRowIndex = this.dgvTables.Rows.Count - 1;

                        }
                        loopround++;
                    }
                    else if (INTindex != strnum & current_stride == "11")
                    {
                        INTindex = strnum;

                        dgvTables.Rows.Add(timedata, loop_counter.ToString(), loopround.ToString(), currentforceMax.ToString(), voltagemark);
                        currentforceMax = 0;
                        totxtstride = string.Concat(loop_counter.ToString(), loopround.ToString().PadLeft(3, '0'));
                        SimpleWriteAsync(totxtstride);
                        current_stride = pushcount_stride;
                        loopround++;
                        voltagemark = null;
                        this.dgvTables.FirstDisplayedScrollingRowIndex = this.dgvTables.Rows.Count - 1;

                    }
                    if (loopround == MaxindexStride + 1)
                    {

                        loopround = 1;
                        loop_counter++;

                    }


                    pushcount = loop_counter;


                }

                if (postarget & nagtarget)
                {
                    label14.Text = (forcenum).ToString();
                    forcenum++;
                    postarget = false;
                    nagtarget = false;
                    currentforceMax = 0;
                }

                if (loop_counter == int.Parse(textBox_time.Text) + 1 & loopround == 1)
                {
                    stopprogram = true;

                    totxtstride = string.Concat(loop_counter.ToString(), loopround.ToString().PadLeft(3, '0'));
                    SimpleWriteAsync(totxtstride);
                }
                if (stopprogram)
                {
                    RunScheduler_timer.Stop();
                    Thread.Sleep(500);

                    byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };
                    serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                    timer_TimeDetect.Stop();
                    CanStart();
                    clear = 0;
                    nowpart = 2;

                    bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A };
                    serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                    this.label_count.Text = "STOP";
                    this.label_count.ForeColor = Color.Red;
                }

            }

        }

        private void RunSchedulerstart()
        {
            timedetect = 0;
            CantStart();
            clear = 0;
            nowpart = 6; //part0 => 下降偵測重量 1=>持續按壓  2>持續感測(校準用) 3>固定距離按壓
            set = 0;
            button_emergencystop.Enabled = true;
            this.rtxtDataArea.Rtf = "";
            serialPort2.Write("stop");
            this.label_count.Text = "START";
            this.label_count.ForeColor = Color.Green;
            this.label_count.Text = "Please wait.";
            this.label8.Text = "Please wait.";
            Thread.Sleep(500);
            RunScheduler_timer.Start();
        }

        private void bt_RunScheduler_Click(object sender, EventArgs e)
        {
            time_cur_weight.Stop();    //turn off stressdetect
            trackerbar_time.Stop();
            if (serialPort2.IsOpen & serialPort.IsOpen)
            {
                byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x38, 0x42, 0x33, 0x0D, 0x0A };  //201e 0008
                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A };  //201e 0003
                serialPort.Write(bytesToSend, 0, bytesToSend.Length);

                RunSchedulerstart();
            }
            else
            {
                MessageBox.Show("請打開串口", "Error");
                return;
            }

        }

        private void dgvTemplates_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {

        }


        private string GetSpecialLines(int startLineNum, int endLineNum)
        {
            string getline;
            //获取指定行的第一行的第一个字符的索引
            if (startLineNum > 0)
            {
                int start = rtxtDataArea.GetFirstCharIndexFromLine(startLineNum - 1);

                //获取指定行的最后一行的第一个字符的索引
                int end = rtxtDataArea.GetFirstCharIndexFromLine(endLineNum);
                rtxtDataArea.Select(start, end - start);//选中指定行
                getline = rtxtDataArea.SelectedText;//设置选择行的内容为空
                return getline;
            }
            else
            {
                return "0";
            }
        }
        private void promahcine(string msg)
        {
            msg = ToyoASCII(msg);
            byte[] byteDIS1 = msg.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(byteDIS1, 0, byteDIS1.Length);

        }

        private void textBox_distance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != (char)('.') && e.KeyChar != (char)('-'))
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)('-'))
            {
                if ((sender as TextBox).Text != "")
                {
                    e.Handled = true;
                }
            }
            //小数点只能1次
            if (e.KeyChar == (char)('.') && ((TextBox)sender).Text.IndexOf('.') != -1)
            {
                e.Handled = true;
            }
            //小数点（最大到2位）
            if (e.KeyChar != '\b' && (((TextBox)sender).SelectionStart) > (((TextBox)sender).Text.LastIndexOf('.')) + 2 && ((TextBox)sender).Text.IndexOf(".") >= 0)
                e.Handled = true;
        }
        int counter = 0;



        private void dgvTemplates_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //1   
            //0index
            //1operation_mode
            //2moving_coordinate
            //3moving_speed
            //4wait_time
            //5force
            //6beused
            //MessageBox.Show(dgvTemplates.CurrentCell.RowIndex.ToString() + dgvTemplates.CurrentCell.ColumnIndex.ToString());
            int current_row = dgvTemplates.CurrentCell.RowIndex;
            int current_column = dgvTemplates.CurrentCell.ColumnIndex;

            string str = "";
            if (dgvTemplates.Rows[current_row].Cells[current_column].Value != null)
                str = dgvTemplates.Rows[current_row].Cells[current_column].Value.ToString();
            var BeusedCell = dgvTemplates.Rows[current_row].Cells[current_column];
            if (current_column == 1)
            {
                ;
            }
            else if (current_column == 2)
            {
                if (double.Parse(str) > 50)
                    BeusedCell.Style.BackColor = Color.Red;
                else
                    BeusedCell.Style.BackColor = Color.White;
            }
            else if (current_column == 3)
            {
                if (double.Parse(str) > 100)
                    BeusedCell.Style.BackColor = Color.Red;
                else
                    BeusedCell.Style.BackColor = Color.White;
            }
            else if (current_column == 4)
            {
                if (double.Parse(str) > 30000)
                    BeusedCell.Style.BackColor = Color.Red;
                else
                    BeusedCell.Style.BackColor = Color.White;
            }
            else if (current_column == 5)
            {
                ;
            }
            else if (current_column == 6)
            {
                if (dgvTemplates.Rows[current_row].Cells[current_column].Value == null)
                    BeusedCell.Style.BackColor = Color.Red;
                else
                    BeusedCell.Style.BackColor = Color.White;
            }
            Graphics g = this.pBar1.CreateGraphics();
            Font font = new Font("Times New Roman", (float)10, FontStyle.Regular);
            PointF pt = new PointF(this.pBar1.Width / 2 - 25, this.pBar1.Height / 2 - 7);
            g.DrawString("re-save", font, Brushes.Red, pt);

        }




        private void button2_Click(object sender, EventArgs e)
        {
            int value = 0;
            serialPort.DiscardInBuffer();
            ////3A 30 31 30 33 31 30 30 38 30 30 30 32 44 44 0D 0A
            byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x33, 0x31, 0x30, 0x30, 0x38, 0x30, 0x30, 0x30, 0x32, 0x45, 0x32, 0x0D, 0x0A };
            serialPort.Write(bytesToSend, 0, bytesToSend.Length);
            Thread.Sleep(100);
            int len = serialPort.BytesToRead;
            byte[] bytes = new byte[len];
            serialPort.Read(bytes, 0, len);


            contents = Encoding.UTF8.GetString(bytes);

            if (contents != null)
            {
                str = contents.Substring(11, 4);

            }

            int intValue = Convert.ToInt32(str, 16);


        }

        private void bt_Exportini_Click(object sender, EventArgs e)
        {
            bt_Exportini.Enabled = false;
            //to inifiles
            string path = System.IO.Directory.GetCurrentDirectory();
            string filename = string.Concat(path, @"\config.ini");
            addScript.ImpostiniFile(filename);
            bt_Exportini.Enabled = true;

            //以下只是為了讀取需要的stride才需要的步驟
            Dictionary<int?, string> frequenciesTable;
            frequenciesTable = new Dictionary<int?, string>();
            StringBuilder ROWSequence = new StringBuilder();

            foreach (DataGridViewRow rowdata in dgvTemplates.Rows)
            {
                if (rowdata.Cells[dgvTemplates.Columns[6].Index].Value.ToString() != "-1")
                {
                    for (int i = 0; i < dgvTemplates.Columns.Count; i++)
                    {
                        string submsg = "";
                        if (i != 1)
                            submsg = rowdata.Cells[i].Value.ToString();
                        else
                            submsg = "1";
                        ROWSequence.Append(string.Concat(submsg, " "));
                    }
                    int NUMS = Convert.ToInt32(rowdata.Cells[dgvTemplates.Columns[6].Index].Value);
                    frequenciesTable[NUMS] = ROWSequence.ToString();
                    ROWSequence.Clear();
                }

            }

            Dictionary<int?, string> result = frequenciesTable.OrderBy(Data => Data.Key).ToDictionary(keyvalue => keyvalue.Key, keyvalue => keyvalue.Value);
            //http://jengting.blogspot.com/2014/07/c-dictionary-value.html
            using (StreamWriter file = new StreamWriter(path + @"\par_.txt"))
            {
                foreach (var entry in result)
                {
                    file.WriteLine("{0}", entry.Value);
                    counter++;
                }
            }
        }
        int ToyoOnOffbin = 0;
        private void ToyoOnOff_Click(object sender, EventArgs e)
        {
            if (ToyoOnOffbin == 1 & serialPort.IsOpen)
            {
                byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x31, 0x30, 0x30, 0x30, 0x30, 0x43, 0x38, 0x0D, 0x0A };
                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                ToyoOnOffbin = 0;

            }
            else if (ToyoOnOffbin == 0 & serialPort.IsOpen)
            {
                byte[] bytesToSend = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x31, 0x30, 0x30, 0x30, 0x31, 0x43, 0x37, 0x0D, 0x0A };
                serialPort.Write(bytesToSend, 0, bytesToSend.Length);
                ToyoOnOffbin = 1;

            }
        }


        public static bool SaveCSV(DataTable dt, string fullPath)
        {
            try
            {
                FileInfo fi = new FileInfo(fullPath);
                if (!fi.Directory.Exists)
                {
                    fi.Directory.Create();
                }
                FileStream fs = new FileStream(fullPath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                //StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
                StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
                string data = "";
                //寫出列名稱
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    data += "\"" + dt.Columns[i].ColumnName.ToString() + "\"";
                    if (i < dt.Columns.Count - 1)
                    {
                        data += ",";
                    }
                }
                sw.WriteLine(data);
                //寫出各行數據
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    data = "";
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        string str = dt.Rows[i][j].ToString();
                        str = string.Format("\"{0}\"", str);
                        data += str;
                        if (j < dt.Columns.Count - 1)
                        {
                            data += ",";
                        }
                    }
                    sw.WriteLine(data);
                }
                sw.Close();
                fs.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void bt_RecordExp_Click(object sender, EventArgs e)
        {

            if (dgvTables.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "CSV (*.csv)|*.csv";
                sfd.FileName = "Output.csv";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            int columnCount = dgvTables.Columns.Count;
                            string columnNames = "";
                            string[] outputCsv = new string[dgvTables.Rows.Count + 1];
                            for (int i = 0; i < columnCount; i++)
                            {
                                columnNames += dgvTables.Columns[i].HeaderText.ToString() + ",";
                            }
                            outputCsv[0] += columnNames;

                            for (int i = 1; (i - 1) < dgvTables.Rows.Count; i++)
                            {
                                for (int j = 0; j < columnCount; j++)
                                {
                                    outputCsv[i] += dgvTables.Rows[i - 1].Cells[j].Value.ToString() + ",";
                                }
                            }

                            File.WriteAllLines(sfd.FileName, outputCsv, Encoding.UTF8);
                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info");
            }
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort2.Write("start");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            serialPort2.Write("start");
        }

        private void Recordtype_yes_CheckedChanged(object sender, EventArgs e)
        {
            if (Recordtype_yes.Checked)
                Recordtype_no.Checked = false;
        }

        private void Recordtype_no_CheckedChanged(object sender, EventArgs e)
        {
            if (Recordtype_no.Checked)
                Recordtype_yes.Checked = false;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button_SearchCOM3_Click(object sender, EventArgs e)
        {
            comboBox_com3.Text = "";
            comboBox_com3.Items.Clear();

            string[] str = SerialPort.GetPortNames();
            if (str == null)
            {
                MessageBox.Show("未偵測到COM Port！", "Error");
                return;
            }

            //添加COM
            foreach (string s in str)
            {
                comboBox_com3.Items.Add(s);
            }

            //設置預設COM
            comboBox_com3.SelectedIndex = 0;
        }

        private void button_buttonOpenCloseCom3_Click(object sender, EventArgs e)
        {

            if (!serialPort3.IsOpen)//COM若關閉
            {
                try
                {
                    if (comboBox_com3.SelectedIndex == -1)
                    {
                        MessageBox.Show("Error: 無效端口,請重新選擇", "Errors");
                        return;
                    }
                    string strSerialName = comboBox_com3.SelectedItem.ToString();
                    string strBaudRate = "115200";
                    string strDataBit = "8";
                    string strCheckBit = "None";
                    string strStopBit = "1";

                    Int32 iBaudRate = Convert.ToInt32(strBaudRate);
                    Int32 iDataBit = Convert.ToInt32(strDataBit);

                    serialPort3.PortName = strSerialName;//COM
                    serialPort3.BaudRate = iBaudRate;//波特率
                    serialPort3.DataBits = iDataBit;//數據位

                    switch (strStopBit)            //停止位
                    {
                        case "1":
                            serialPort3.StopBits = StopBits.One;
                            break;
                        case "1.5":
                            serialPort3.StopBits = StopBits.OnePointFive;
                            break;
                        case "2":
                            serialPort3.StopBits = StopBits.Two;
                            break;
                        default:
                            MessageBox.Show("Error：停止位參數錯誤!", "Error");
                            break;
                    }
                    switch (strCheckBit)             //校驗位
                    {
                        case "None":
                            serialPort3.Parity = Parity.None;
                            break;
                        case "Odd":
                            serialPort3.Parity = Parity.Odd;
                            break;
                        case "Even":
                            serialPort3.Parity = Parity.Even;
                            break;
                        default:
                            MessageBox.Show("Error：教驗位參數錯誤!", "Error");
                            break;
                    }

                    if (saveDataFile != null)
                    {
                        saveDataFS = File.Create(saveDataFile);
                    }

                    //打開串口
                    serialPort3.Open();
                    //打開串口後設置變無效
                    comboBox_com3.Enabled = false;
                    button_buttonOpenCloseCom3.Text = "Close COM";
                    label_COMconnect3.Text = "Connect Pass";
                    label_COMconnect3.ForeColor = Color.Green;

                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("Error:" + ex.Message, "Error");
                    return;
                }
            }
            else //COM處於打開
            {
                serialPort3.Close();//關閉串口
                //串口关闭时设置有效
                comboBox_com3.Enabled = true;
                button_buttonOpenCloseCom3.Text = "Open COM";
                label_COMconnect3.Text = "No Connect";
                label_COMconnect3.ForeColor = Color.Red;
                if (saveDataFS != null)
                {
                    saveDataFS.Close(); // 關閉完建
                    saveDataFS = null;//釋放文件句柄
                }
            }
        }

        private static string Reverse(string content)
        {
            char[] charArray = content.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }



        private void trackBar1_Scroll(object sender, EventArgs e)
        {

            //if (tb_lenInterval.Text.Trim() == null)
            //{
            //    nUD_curStress.Increment = Convert.ToDecimal("0.03");
            //    trackBar1.LargeChange = (int)(Convert.ToDouble(tb_lenInterval.Text) * 100);
            //}
            //double a = Math.Round((5000 - (double)trackBar1.Value) / 100, 2);
            //nUD_curStress.Text = a.ToString();
            //trackBar1.ValueChanged += new EventHandler(this.trackBar1_ValueChanged);
        }
        private void trackBar1_ValueChanged(Object sender, EventArgs e)
        {
            double a = Math.Round((5000 - (double)trackBar1.Value) / 100, 2);
            nUD_curStress.Text = a.ToString();
            bt_WT_calibration.PerformClick();
            //trackBar1.Value = Convert.ToInt32(5000 - (nUD_curStress.Value * 100));
        }
        decimal decimalint = 0;


        private void tb_lenInterval_Leave(object sender, EventArgs e)
        {

            if (tb_lenInterval.Text.Trim() == null)
            {
                tb_lenInterval.Text = "0.03";
                nUD_curStress.Increment = Convert.ToDecimal("0.03");
                trackBar1.LargeChange = (int)(Convert.ToDouble(tb_lenInterval.Text) * 100);

            }
            else
            {
                try
                {
                    nUD_curStress.Increment = Convert.ToDecimal(tb_lenInterval.Text);
                    decimalint = Convert.ToDecimal(tb_lenInterval.Text);

                    if (decimalint > 1)
                        tb_lenInterval.Text = "1";
                }
                catch
                {
                    MessageBox.Show("don't null!");
                    nUD_curStress.Increment = Convert.ToDecimal("0.03");
                }



            }

        }
        private void nUD_curStress_ValueChanged(object sender, EventArgs e)
        {
            if (tb_lenInterval.Text.Trim() == null)
            {

                tb_lenInterval.Text = "0.03";
                nUD_curStress.Increment = Convert.ToDecimal("0.03");
                trackBar1.LargeChange = (int)(Convert.ToDouble(tb_lenInterval.Text) * 100);
            }
            else
            {
                try
                {
                    nUD_curStress.Increment = Convert.ToDecimal(tb_lenInterval.Text);
                    decimalint = Convert.ToDecimal(tb_lenInterval.Text);

                    if (decimalint > 1)
                        tb_lenInterval.Text = "1";
                }
                catch
                {
                    MessageBox.Show("don't null!");
                    nUD_curStress.Increment = Convert.ToDecimal("0.03");
                }
            }

            //label22.Text = Convert.ToString( nUD_curStress.Value );
            trackBar1.Value = Convert.ToInt32(5000 - (nUD_curStress.Value * 100));
            if (serialPort.IsOpen)
            {
                string distance = (double.Parse(nUD_curStress.Text) * 100).ToString();       //Max 50
                distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
                distance = ASCIIToHex(distance);
                string distancecommand;
                double distanceInt = double.Parse(nUD_curStress.Text) * 100;

                int delay = int.Parse(textBox_delaytime.Text);

                if (distanceInt >= 4096)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 " + distance;            //連續讀寫 10 2002 0002 04 0000 xxxx
                }
                else if (distanceInt < 4096 && distanceInt >= 256)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 " + distance;         //連續讀寫 10 2002 0002 04 0000 0xxx
                }
                else if (distanceInt < 256 && distanceInt >= 16)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 " + distance;      //連續讀寫 10 2002 0002 04 0000 00xx
                }
                else
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance;   //連續讀寫 10 2002 0002 04 0000 000x
                }
                distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
                distancecommand = ToyoASCII(distancecommand);
                byte[] start = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x31, 0x42, 0x41, 0x0D, 0x0A };     // 06 201E 0001 #ABS絕對位置移動
                serialPort.Write(start, 0, start.Length);

                byte[] bytedistance = distancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();     //00 01 02 03 ->0x0 0x1

                serialPort.Write(bytedistance, 0, bytedistance.Length);
                //Thread.Sleep(100);
                Thread.Sleep(200);
                serialPort.Write(start, 0, start.Length);
                serialPort.Write(bytedistance, 0, bytedistance.Length);

            }



        }

        private void tb_lenInterval_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((int)e.KeyChar < 48 | (int)e.KeyChar > 57) & (int)e.KeyChar != 8 & (int)e.KeyChar != 46)
            {
                e.Handled = true;
            }
        }





        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void nUD_curStress_EnabledChanged(object sender, EventArgs e)
        {
            if (tb_lenInterval.Text.Trim() == null | Convert.ToDecimal(tb_lenInterval.Text) > 1)
            {

                tb_lenInterval.Text = "0.03";
                nUD_curStress.Increment = Convert.ToDecimal("0.03");
                trackBar1.LargeChange = (int)(Convert.ToDouble(tb_lenInterval.Text) * 100);
            }
        }

        bool WT_flag = false;
        decimal Anchorpoint = (decimal)0.0;
        decimal moving_interval = (decimal)0.0;
        decimal moving_freq = (decimal)0.0;

        UInt16 WT_process_index = 0;
        UInt16 nowpartpro = 0;
        private void bt_Weightdetect_Click(object sender, EventArgs e)
        {
            if (!WT_flag & cb_wt_detectpress.Checked)
            {
                WT_flag = true;
                Anchorpoint = nUD_curStress.Value;
                //serialPort2.Write("Stop");
                trackerbar_time.Stop();
                WT_process_index = 3;
                serialPort2.DataReceived += SerialPortDataReceived2;
                bt_wt_lock.Enabled = false;

                if (cccsetform_OnOff)
                {
                    SendValueCallback("C0 02 01 08 73 74 6F 70");
                }
                  

            }
            else
            {
                trackerbar_time.Start();
                //serialPort2.Write("start");
                time_cur_weight.Stop();

                WT_process_index = 4;
                WT_flag = false;
                //serialPort2.DataReceived += null;
                bt_wt_lock.Enabled = true;

                if (cccsetform_OnOff)
                {
                    SendValueCallback("C0 02 01 08 61 62 6C 65");
                }
             
            }
            bt_Weightdetect.Text = WT_flag == true ? "Stop" : "Detect";


        }
        int WT_savecsv_clear = 1;
        int WT_saveimg_clear = 0;

        private void bt_ClearWTlog_Click(object sender, EventArgs e)
        {
            wt_loop_counter = 1;
            WT_process_index = 1;
            dgvTables_wt.Rows.Clear();
        }
        decimal[] xs = new decimal[6];
        UInt16 WT_cortime = 1;

        decimal Median(decimal[] _xs)
        {
            Array.Sort(xs);
            return xs[xs.Length / 2];
        }
        decimal Average(decimal _xs, int _num)
        {
            return  _xs / WT_cortime;
        }
        private void moveCMD(string cmg)
        {
            moveCMD_text(cmg);
        }
        private void moveCMD(decimal cmg)
        {
            string intcmg = cmg.ToString();
            moveCMD_text(intcmg);
        }
        private void moveCMD_text(string cmg)
        {
            //if(cmg == "0.00")
            //    threshold_flag = 0;

            if (serialPort.IsOpen)
            {
                serialPort.DiscardInBuffer();
                serialPort.DiscardOutBuffer();
                string distance = (double.Parse(cmg) * 100).ToString();       //Max 50
                distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
                distance = ASCIIToHex(distance);
                string distancecommand;
                double distanceInt = double.Parse(cmg) * 100;

                int delay = int.Parse(textBox_delaytime.Text);

                if (distanceInt >= 4096)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 " + distance;            //連續讀寫 10 2002 0002 04 0000 xxxx
                }
                else if (distanceInt < 4096 && distanceInt >= 256)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 " + distance;         //連續讀寫 10 2002 0002 04 0000 0xxx
                }
                else if (distanceInt < 256 && distanceInt >= 16)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 " + distance;      //連續讀寫 10 2002 0002 04 0000 00xx
                }
                else
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance;   //連續讀寫 10 2002 0002 04 0000 000x
                }
                distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
                distancecommand = ToyoASCII(distancecommand);
                byte[] start = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x31, 0x42, 0x41, 0x0D, 0x0A };     // 06 201E 0001 #ABS絕對位置移動
                serialPort.Write(start, 0, start.Length);

                byte[] bytedistance = distancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();     //00 01 02 03 ->0x0 0x1

                serialPort.Write(bytedistance, 0, bytedistance.Length);

                Task.Delay(50);
                serialPort.Write(start, 0, start.Length);
                serialPort.Write(bytedistance, 0, bytedistance.Length);

            }

        }
        private void trackerbar_time_Tick(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                tracepoint();
            }

            //if (nUD_curStress.Value == Convert.ToDecimal(label22.Text) & control_tracker_flag & serialPort.IsOpen)
            //{
            //    bt_Weightdetect.Enabled = true;
            //}
            //else
            //{
            //    bt_Weightdetect.Enabled = false;
            //}
        }
        void tracepoint()
        {
            string slcmg = null;
            slcmg = "30 33 31 30 30 38 30 30 30 32";
            slcmg = ToyoASCII(slcmg.Trim());

            byte[] byte_slcmg = slcmg.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(byte_slcmg, 0, byte_slcmg.Length);
            Task.Delay(100);

            int len = serialPort.BytesToRead;
            byte[] bytes = new byte[len];
            serialPort.Read(bytes, 0, len);
            string bufferls = null;
            bufferls = System.Text.Encoding.Default.GetString(bytes);     //byte trans to ascii

            if (bufferls.Length == 19)
            {

                try 
                {
                    string hex_len = bufferls.Substring(11, 4);
                    int value = Convert.ToInt32(hex_len, 16);
                    double double_value = Convert.ToDouble(value) / 100;

                    if (!WT_flag)
                    {
                        label22.Text = double_value.ToString();
                        label27.Text = double_value.ToString();
                    }

                    else
                    {
                        label27.Text = double_value.ToString();
                    }
                }
                catch 
                {
                    string hex_len = "00";
                    int value = Convert.ToInt32(hex_len, 16);
                    double double_value = Convert.ToDouble(value) / 100;

                    if (!WT_flag)
                    {
                        label22.Text = double_value.ToString();
                        label27.Text = double_value.ToString();
                    }

                    else
                    {
                        label27.Text = double_value.ToString();
                    }
                    WT_process_index = 1;
                }
           

            }
        }
 

        private void bt_WT_calibration_Click(object sender, EventArgs e)
        {
            //trackBar1.Value = Convert.ToInt32(5000 - (nUD_curStress.Value * 100));
            trackerbar_time.Stop();

            if (serialPort.IsOpen)
            {
                serialPort.DiscardInBuffer();
                serialPort.DiscardOutBuffer();
                string distance = (double.Parse(nUD_curStress.Text) * 100).ToString();       //Max 50
                distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
                distance = ASCIIToHex(distance);
                string distancecommand;
                double distanceInt = double.Parse(nUD_curStress.Text) * 100;

                int delay = int.Parse(textBox_delaytime.Text);

                if (distanceInt >= 4096)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 " + distance;            //連續讀寫 10 2002 0002 04 0000 xxxx
                }
                else if (distanceInt < 4096 && distanceInt >= 256)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 " + distance;         //連續讀寫 10 2002 0002 04 0000 0xxx
                }
                else if (distanceInt < 256 && distanceInt >= 16)
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 " + distance;      //連續讀寫 10 2002 0002 04 0000 00xx
                }
                else
                {
                    distancecommand = "31 30 32 30 30 32 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance;   //連續讀寫 10 2002 0002 04 0000 000x
                }
                distancecommand = distancecommand.Substring(0, distancecommand.Length - 1);//消除最後一位(" ")
                distancecommand = ToyoASCII(distancecommand);
                byte[] start = new byte[] { 0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x31, 0x42, 0x41, 0x0D, 0x0A };     // 06 201E 0001 #ABS絕對位置移動
                serialPort.Write(start, 0, start.Length);

                byte[] bytedistance = distancecommand.Split().Select(s => Convert.ToByte(s, 16)).ToArray();     //00 01 02 03 ->0x0 0x1

                serialPort.Write(bytedistance, 0, bytedistance.Length);
                //Thread.Sleep(100);
                Thread.Sleep(50);
                serialPort.Write(start, 0, start.Length);
                serialPort.Write(bytedistance, 0, bytedistance.Length);

            }
            //Task.Delay(200);

            //if (serialPort.IsOpen)
            //{
            //    string slcmg = null;
            //    slcmg = "30 33 31 30 30 38 30 30 30 32";
            //    //3A 30 31 30 33 31 30 30 38 30 30 30 32 45 32 0D 0A
            //    slcmg = ToyoASCII(slcmg.Trim());

            //    byte[] byte_slcmg = slcmg.Split().Select(s => Convert.ToByte(s, 16)).ToArray();     //00 01 02 03 ->0x0 0x1

            //    serialPort.Write(byte_slcmg, 0, byte_slcmg.Length);
            //    Task.Delay(100);

            //    int len = serialPort.BytesToRead;
            //    byte[] bytes = new byte[len];
            //    serialPort.Read(bytes, 0, len);
            //    string bufferls = null;
            //    bufferls = System.Text.Encoding.Default.GetString(bytes);     //byte trans to ascii
            //    label23.Text = bufferls.ToString();
            //    if (bufferls.Length == 19)
            //    {
            //        string hex_len = bufferls.Substring(11, 4);
            //        int value = Convert.ToInt32(hex_len, 16);
            //        double double_value = Convert.ToDouble(value) / 100;

            //    }

            //    listBox1.Items.Add(bufferls);
            //    listBox1.SelectedIndex = listBox1.Items.Count - 1;
            //}
            trackerbar_time.Start();
        }
        bool control_tracker_flag = false;
        private void bt_WT_lock_Click(object sender, EventArgs e)
        {

        }
        string bufferlsa = null;
        private void SerialPortDataReceived2(object sender, SerialDataReceivedEventArgs e)
        {
            var serialPort = (SerialPort)sender;
            var data = serialPort.ReadExisting();
            SetText2(data);

        }

        int wt_delaytime = 1000;
        int wt_delaycount = 1;

        decimal deci_current_weight = 0;
        decimal deci_current_weight_one = 0;
        decimal deci_temple_weight = 0;
        UInt16 threshold_flag = 0;
        string counmsg = null;
        string new_msg = null;

        private void SetText2(string text)
        {
            if (serialPort2.IsOpen)
            {

                if (this.rtxtDataArea.InvokeRequired)
                {

                    SetTextCallback_WT d = new SetTextCallback_WT(SetText2);
                    this.Invoke(d, new object[] { text });
                }
                else
                {

                    this.rtxtDataArea.Focus(); //让文本框获取焦点 
                    this.rtxtDataArea.Select(this.rtxtDataArea.TextLength, 0);//设置光标的位置到文本尾
                    this.rtxtDataArea.ScrollToCaret();//滚动到控件光标处 
                    this.rtxtDataArea.AppendText(text);
                    //tracepoint();

                    if (WT_process_index == 0)      //collect the pressture number
                    {
                        listBox4.Items.Add(WT_process_index.ToString());
                        listBox4.SelectedIndex = listBox4.Items.Count - 1;

                        gettext = GetSpecialLines(Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1, Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1);
                        label23.Text =  gettext.ToString();
                        listBox3.Items.Add(gettext);
                        listBox3.SelectedIndex = listBox3.Items.Count - 1;

                        if (WT_flag)
                        {

                            try
                            {
                                deci_current_weight = Convert.ToDecimal(gettext);//uintdec);// 
                            }
                            catch
                            {
                                deci_current_weight = Convert.ToDecimal("0.1");
                            }

                            deci_temple_weight = Convert.ToDecimal(tb_templeWeight.Text);
                            string timedata = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            //label28.Text = deci_current_weight.ToString();

                            ///check each point weight if the weight is enough 
                            if (deci_current_weight > deci_temple_weight & threshold_flag == 0)
                            {
                                time_cur_weight.Stop();
                                threshold_flag++;
                               
                                deci_current_weight_one = deci_current_weight + (decimal)0.001;
                                dgvTables_wt.Rows.Add(timedata, wt_loop_counter.ToString(), deci_current_weight_one.ToString(), "");
                                deci_current_weight_one = 0;
                                this.dgvTables_wt.FirstDisplayedScrollingRowIndex = this.dgvTables_wt.Rows.Count - 1;

                                if (cccsetform_OnOff)
                                {

                                    if (Convert.ToInt16(tb_freq_saveimgimg.Text) == WT_saveimg_clear )
                                    {
                                        recorddatatoimg();
                                        WT_saveimg_clear++;
                                    }
                                    else if ( Convert.ToInt16(tb_freq_saveimgimg.Text) != WT_saveimg_clear)
                                    {
                                        WT_saveimg_clear++;
                                    }
                                }
                                wt_loop_counter++;                                //data update
                                
                                listBox2.Items.Add(Convert.ToInt16(tb_freq_saveimgcsv.Text).ToString());
                                listBox2.SelectedIndex = listBox2.Items.Count - 1;
                             
                                if(  Convert.ToInt16(tb_freq_saveimgcsv.Text) == WT_savecsv_clear) 
                                {
                                    recorddatatocsv();
                                    dgvTables_wt.Rows.Clear();
                                    WT_savecsv_clear++;
                                }
                                else if (wt_loop_counter != 1  & Convert.ToInt16(tb_freq_saveimgcsv.Text) != WT_savecsv_clear) 
                                {
                                    WT_savecsv_clear++;
                                }
                              

                                moving_freq = 0;
                                wt_delaycount = 1;
                                wt_delaytime = 900;
                                time_cur_weight.Interval = wt_delaytime;
                                deci_current_weight = 0;

                                WT_process_index = 1;
                               
                            }
                            deci_current_weight_one = 0;

                            if (Convert.ToInt32(tb_pushtimes.Text) < wt_loop_counter)
                            {
                                bt_Weightdetect.PerformClick();

                                moving_freq = 0;
                                wt_delaycount = 1;
                                wt_delaytime = 900;
                                time_cur_weight.Interval = wt_delaytime;
                                deci_current_weight = 0;


                                WT_process_index = 6;
                                wt_loop_counter = 1;
                                button_ZeroDistance.PerformClick();
                            }
                            
                        }
                

                    }               
                    else if (WT_process_index == 1)
                    {
                        //trace_report_time.Start();

                        listBox4.Items.Add(WT_process_index.ToString());
                        listBox4.SelectedIndex = listBox4.Items.Count - 1;

                        threshold_flag = 0;

                        //serialPort2.Write("start");
                        doWTest1_GOtoAnchorPoint();

                        idle_time.Start();
                        if (GOtoORG_flag)
                        {
                            WT_process_index = 3;
                            GOtoORG_flag = false;
                        } 
                    }
                    else if (WT_process_index == 3)
                    {
                        idle_time.Stop();
                        listBox4.Items.Add(WT_process_index.ToString());
                        listBox4.SelectedIndex = listBox4.Items.Count - 1;

                        doWTest2_GOtoFitPoint();
                        time_cur_weight.Start();
                        wt_delaytime = Exper(wt_delaycount);
                        time_cur_weight.Interval = wt_delaytime;
                        wt_delaycount++;
                        WT_process_index = 0;
                       

                    }
                    else if (WT_process_index == 4)     //record
                    {
                        listBox4.Items.Add(WT_process_index.ToString());
                        listBox4.SelectedIndex = listBox4.Items.Count - 1;

                        gettext = GetSpecialLines(Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1, Regex.Split(this.rtxtDataArea.Text, "\n").Length - 1);
                        label23.Text = gettext.ToString();
                        listBox3.Items.Add(gettext);
                        listBox3.SelectedIndex = listBox3.Items.Count - 1;

                    }
                    else if (WT_process_index == 5)     //start
                    {
                        //serialPort2.Write("start");
                       
                    }
                    else if (WT_process_index == 6)     //stop
                    {
                        //serialPort2.Write("stop");
                    }
                    else if (WT_process_index == 7)     //capture
                    {
                        /// C0 02 02 08 00 12 34 56
                        //SendValueCallback("C0 02 02 08 00 12 34 56");
                        if (cccsetsaveimg)
                            wt_delaycount = 0;

                    }
                    else if (WT_process_index == 9)
                    {


                    }
                    else
                    {; }

                    if (!WT_flag)
                    {; }
                        //serialPort2.Write("start");


                }
            }

        }
        void recorddatatocsv( )
        {
            string timedata = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace(":","_");
         
            if (true  )
            {
                serialPort2.Write("stop");

                WriteToCSV(img_path + timedata + ".csv", dgvTables_wt);
                WT_savecsv_clear = 0;
                rtxtDataArea.Rtf = "";
                rtxtDataArea.Clear();
                listBox1.Items.Clear();
                listBox2.Items.Clear();
                listBox3.Items.Clear();
                listBox4.Items.Clear();
                serialPort2.Write("start");
                WT_process_index = 1;

            }
          
        }
        private void idle_time_Tick(object sender, EventArgs e)
        {
            GOtoORG_flag = true;
        }
        void recorddatatoimg()
        {
            string timedata = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace(":", "_");

            if (true)
            {
                // C0 02 02 08 00 12 34 56
                counmsg = wt_loop_counter.ToString().PadLeft(8, '0');

                for (int i = 0; i < 8; i += 2)
                {
                    new_msg += counmsg.Substring(i, 2) + " ";
                }
                SendValueCallback("C0 02 02 08 " + new_msg.Trim());
                new_msg = null;
                WT_saveimg_clear = 0;

            }

        }
        int Exper(int _exp)
        {
            double _double_exp = Convert.ToInt32(_exp);
            int delaytime =Convert.ToInt32( Math.Pow(1.23, _double_exp)*800);
            return delaytime;
        }
        int Exper2(int _exp)
        {
            double _double_exp = Convert.ToInt32(_exp);
            int delaytime = Convert.ToInt32(Math.Pow(2.718, _double_exp) * 200);
            return delaytime;
        }
        void WriteToCSV(string FilePath, DataGridView dagtv)
        {
            if (dgvTables_wt.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();

                bool fileError = false;

                if (!fileError)
                {
                    try
                    {
                        int columnCount = dgvTables_wt.Columns.Count;
                        string columnNames = "";
                        string[] outputCsv = new string[dgvTables_wt.Rows.Count + 1];
                        for (int i = 0; i < columnCount; i++)
                        {
                            columnNames += dgvTables_wt.Columns[i].HeaderText.ToString() + ",";
                        }
                        outputCsv[0] += columnNames;

                        for (int i = 1; (i - 1) < dgvTables_wt.Rows.Count; i++)
                        {
                            for (int j = 0; j < columnCount; j++)
                            {
                                outputCsv[i] += dgvTables_wt.Rows[i - 1].Cells[j].Value.ToString() + ",";
                            }
                        }

                        File.WriteAllLines(FilePath, outputCsv, Encoding.UTF8);
                        //MessageBox.Show("Data Exported Successfully !!!", "Info");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error :" + ex.Message);
                    }
                }

            }
        }

        double getcoordinate(string _msg)
        {
            string slcmg = null;
            double double_value = 0;
            slcmg = "30 33 31 30 30 38 30 30 30 32";
            slcmg = ToyoASCII(slcmg.Trim());

            byte[] byte_slcmg = slcmg.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            serialPort.Write(byte_slcmg, 0, byte_slcmg.Length);
            Task.Delay(100);

            int len = serialPort.BytesToRead;
            byte[] bytes = new byte[len];
            serialPort.Read(bytes, 0, len);
            string bufferls = null;
            bufferls = System.Text.Encoding.Default.GetString(bytes);     //byte trans to ascii

            if (bufferls.Length == 19)
            {

                try
                {
                    string hex_len = bufferls.Substring(11, 4);
                    int value = Convert.ToInt32(hex_len, 16);
                    double_value = Convert.ToDouble(value) / 100;
                }
                catch
                {
                    
                    int value = 0;
                    double_value = Convert.ToDouble(value) / 100;
                }
       

            }
            return double_value;
        }
        double dynamic_cur_coordinate = 0;
        double dynamic_target_coordinate = 0.1;

        bool GOtoORG_flag = false;
        UInt16 nextstep_flag = 0;
        private void trace_report_time_Tick(object sender, EventArgs e)
        {
            //tracepoint();
            dynamic_cur_coordinate = getcoordinate(null);

            //因為怕tc100會有誤差所以採用小於該目標值
            listBox4.Items.Add("__"+nextstep_flag.ToString());
            listBox4.SelectedIndex = listBox4.Items.Count - 1;


            if (dynamic_cur_coordinate <= Convert.ToDouble(Anchorpoint) | nextstep_flag<20)
            {
                nextstep_flag++;
            }
            if (nextstep_flag >= 20)
            {
                threshold_flag = 0;
                //GOtoORG_flag = true;
               
                serialPort2.Write("stop");
                nextstep_flag = 0;
                trace_report_time.Stop();
                WT_process_index = 3;
             
            }
        
        }
        private void doWTest0_GOtoORG()
        {
            moveCMD("0.00");
            moving_freq = 0;
        }
        private void doWTest1_GOtoAnchorPoint()
        {
            moveCMD(Anchorpoint);
            threshold_flag = 0;
        }
        int wt_loop_counter = 1;
        private void doWTest2_GOtoFitPoint()
        {
            decimal _anchorpoint = Anchorpoint;
            decimal _interval = Convert.ToDecimal(tb_lenInterval.Text);

            if ( serialPort2.IsOpen)
            {

                moving_freq += _interval;
                moveCMD(Anchorpoint + moving_freq);
                listBox2.Items.Add(Anchorpoint + moving_freq);
                listBox2.SelectedIndex = listBox2.Items.Count - 1;

            }
        }
        static void sleepmoment_CMD(int sleeptime)
        {
            System.Threading.Thread.Sleep(sleeptime);
        }
        private void time_cur_weight_Tick(object sender, EventArgs e)
        {
            WT_process_index = 3;
            time_cur_weight.Stop();
        }
        cccSet cccsetform;
        public delegate void SendValueDelegate(string pValue);
        public event SendValueDelegate SendValueCallback;

        private void SetReturnValueCallbackFun(string pValue)
        {
            //MessageBox.Show("slave feedback"+pValue);
            if (pValue == "C1 01 00 08 4F 4E 00 00")
            {
                cccsetform_OnOff = true;
            }
            else if (pValue == "C1 01 00 08 4F 46 46 00")
            {
                cccsetform_OnOff = false;
            }
            else if (pValue == "C1 02 02 08 44 4F 4E 45")
            {
                /// img  save img DONE
                /// C1 02 02 08 44 4F 4E 45 -> DONE
                cccsetsaveimg = true;
            }
            else if (pValue == "C1 01 00 08 4F 46 46 00")
            {
                cccsetform = new cccSet();
            }
            else
            {
                MessageBox.Show("CCCSET" + "傳送某些東西 ->" + pValue);
            }
        }

        private void bt_cccsetform_Click(object sender, EventArgs e)
        {
            if (!cccsetform_OnOff)
            {
                cccsetform = new cccSet();
                cccsetform.Show();
                this.SendValueCallback += new SendValueDelegate(cccsetform.ReceiveValueCallbackFun);

                cccsetform.ReturnValueCallback += new cccSet.ReturnValueDelegate(this.SetReturnValueCallbackFun);
                /// form status
                /// C0 01 00 08 4D 4F 44 45 -> ON
                this.SendValueCallback("C0 01 00 08 4D 4F 44 45");
            }
            else
            {
                MessageBox.Show("該視窗已打開 ~~");
            }

        }

        private void bt_wt_savelog_Click(object sender, EventArgs e)
        {
            if (dgvTables_wt.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "CSV (*.csv)|*.csv";
                sfd.FileName = "Output.csv";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            int columnCount = dgvTables_wt.Columns.Count;
                            string columnNames = "";
                            string[] outputCsv = new string[dgvTables_wt.Rows.Count + 1];
                            for (int i = 0; i < columnCount; i++)
                            {
                                columnNames += dgvTables_wt.Columns[i].HeaderText.ToString() + ",";
                            }
                            outputCsv[0] += columnNames;

                            for (int i = 1; (i - 1) < dgvTables_wt.Rows.Count; i++)
                            {
                                for (int j = 0; j < columnCount; j++)
                                {
                                    outputCsv[i] += dgvTables_wt.Rows[i - 1].Cells[j].Value.ToString() + ",";
                                }
                            }

                            File.WriteAllLines(sfd.FileName, outputCsv, Encoding.UTF8);
                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
        }

        private void bt_wt_lock_Click(object sender, EventArgs e)
        {
            if (nUD_curStress.Value == Convert.ToDecimal(label22.Text) & !control_tracker_flag)
            {
                control_tracker_flag = true;
                trackBar1.Enabled = false;
                nUD_curStress.Enabled = false;
                bt_WT_calibration.Enabled = false;
                bt_Weightdetect.Enabled = true;
                WT_process_index = 0;
            }
            else
            {
                control_tracker_flag = false;
                trackBar1.Enabled = true;
                nUD_curStress.Enabled = true;
                bt_WT_calibration.Enabled = true;
                bt_Weightdetect.Enabled = false;
                WT_process_index = 4;
            }
            bt_wt_lock.Text = control_tracker_flag == true ? "disconnect" : "connect";


        }

        private void button5_Click_1(object sender, EventArgs e)
        {


        }

        private void cb_wt_detectpress_CheckedChanged(object sender, EventArgs e)
        {

            if (serialPort2.IsOpen)
            {
                serialPort2.DataReceived += SerialPortDataReceived2;
                if (cb_wt_detectpress.Checked)
                {
                    serialPort2.Write("stop");
                    enablewtbutton(true);
                }
                else
                {
                    serialPort2.Write("start");
                    enablewtbutton(false);
                }
            }
            else
            {
                MessageBox.Show("check the comport 2~~~");
            }

        }
        void enablewtbutton(bool _flag)
        {
            bt_Weightdetect.Enabled = _flag;
            tb_templeWeight.Enabled = _flag;
            tb_pushtimes.Enabled = _flag;
            trackBar1.Enabled = _flag;
            tb_lenInterval.Enabled = _flag;
            nUD_curStress.Enabled = _flag;
            bt_WT_calibration.Enabled = _flag;
            bt_wt_lock.Enabled = _flag;
            bt_wt_savelog.Enabled = _flag;
            //bt_cccsetform.Enabled = _flag;
            tb_freq_saveimgcsv.Enabled = _flag;
            bt_ClearWTlog.Enabled = _flag;
        }
        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            serialPort2.Write("start");
        }

        private void button5_Click_2(object sender, EventArgs e)
        {
            MessageBox.Show(Convert.ToInt32(tb_freq_saveimgcsv.Text).ToString());
        }

    

        string berecordedPoint = "";
        public void runprogram()
        {
            string par_path = System.IO.Directory.GetCurrentDirectory();
            string[] lines = File.ReadAllLines(par_path + @"\par_.txt");
            //MessageBox.Show(lines.Length.ToString());

            //loading the txt

            StreamReader str = new StreamReader(par_path + @"\par_.txt");
            TransfCommandTC transf = new TransfCommandTC();
            string line;
            int endcount = 1;

            while ((line = str.ReadLine()) != null)
            {
                transf.stride_index = endcount;
                string[] par_ = line.Split(' ');
                if (par_[7].ToUpper().ToUpper() == "Y")
                {
                    berecordedPoint = berecordedPoint + "Y ";

                }
                else
                {
                    berecordedPoint = berecordedPoint + "N ";
                }

                transf.stride_Mode = par_[1];
                transf.stride_distance = par_[2];
                transf.stride_speed = par_[3];
                transf.stride_delaytime = par_[4];
                transf.stride_force = par_[5];
                if (endcount == lines.Length)
                    transf.stride_nextstride = "1";
                else
                    transf.stride_nextstride = (int.Parse(par_[6]) + 1).ToString();


                serialPort.Write(transf.GetModemsg(), 0, transf.GetModemsg().Length);
                serialPort.Write(transf.GetDistancemsg(), 0, transf.GetDistancemsg().Length);
                serialPort.Write(transf.GetSpeed(), 0, transf.GetSpeed().Length);
                serialPort.Write(transf.GetDelaytime(), 0, transf.GetDelaytime().Length);
                serialPort.Write(transf.GetNextStride(), 0, transf.GetNextStride().Length);
                string result = "";
                // result = System.Text.Encoding.UTF8.GetString(transf.GetModemsg());
                //MessageBox.Show(result);
                //result = System.Text.Encoding.UTF8.GetString(transf.GetDistancemsg());
                //MessageBox.Show(result);
                //result = System.Text.Encoding.UTF8.GetString(transf.GetSpeed());
                //MessageBox.Show(result);
                //result = System.Text.Encoding.UTF8.GetString(transf.GetDelaytime());
                //MessageBox.Show(result);
                //result = System.Text.Encoding.UTF8.GetString(transf.GetNextStride());
                //MessageBox.Show(result);
                endcount++;

            }
            MaxindexStride = 0;
            MAXARRAY = berecordedPoint.Trim().Split(' ');
            MaxindexStride = endcount-1;
            str.Close();
        }

        public class IniFile
        {
            private string filePath;
            private StringBuilder lpReturnedString;
            private int bufferSize;

            [DllImport("kernel32")]
            private static extern long WritePrivateProfileString(string section, string key, string lpString, string lpFileName);

            [DllImport("kernel32")]
            private static extern int GetPrivateProfileString(string section, string key, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

            public IniFile(string iniPath)
            {
                filePath = iniPath;
                bufferSize = 512;
                lpReturnedString = new StringBuilder(bufferSize);
            }

            // read ini date depend on section and key
            public string ReadIniFile(string section, string key, string defaultValue)
            {
                lpReturnedString.Clear();
                GetPrivateProfileString(section, key, defaultValue, lpReturnedString, bufferSize, filePath);
                return lpReturnedString.ToString();
            }

            // write ini data depend on section and key
            public void WriteIniFile(string section, string key, Object value)
            {
                WritePrivateProfileString(section, key, value.ToString(), filePath);
            }


        }
    }




}
