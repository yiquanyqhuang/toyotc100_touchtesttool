﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.Util;
using DirectShowLib;
using System.IO;
using System.Threading;
using System.Drawing.Imaging;

namespace ToyoServoCylinderTool
{
   

    public partial class cccSet : Form
    {
        public delegate void ReturnValueDelegate(string pValue);
        public event ReturnValueDelegate ReturnValueCallback;
        Graphics GraphicsTimes;
        private VideoCapture[] _capture = null;
        private bool _captureInProgress = false;
        private Mat _frame;

        private Mat _grayFrame;
        public int[] _camWidth = null;//640;
        public int[] _camHeight = null;//480;
        public int camWidth = 640;
        public int camHeight = 480;
        DsDevice[] devices;
        public int camindex = 0;
        public int selectCamindex = 0;
        string img_path = System.Environment.CurrentDirectory;
        Form1 form1;
        private void DoubleBufferOn()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            UpdateStyles();
        }
    public void ReceiveValueCallbackFun(string pValue)
        {
            //textBox1.Text = pValue;
            if (pValue == "C0 02 02 08 53 41 56 45")
            {
                bt_saveimage.PerformClick();
            }
            else if (pValue == "C0 01 00 08 4D 4F 44 45")
            {
                //MessageBox.Show("form status"+pValue);
                ReturnValueCallback("C1 01 00 08 4F 4E 00 00");
            }
            else if (pValue == "C0 02 01 08 73 74 6F 70")           //form disable
            {
                funlock(false);
            }
            else if (pValue == "C0 02 01 08 61 62 6C 65")           //form enable
            {
                funlock(true);
            }
            else if (pValue.Replace(" ", "").Substring(0,8)== "C0020208")
            {
                //string mStr = str.Replace(" ", "");
                string daytime = System.Text.RegularExpressions.Regex.Replace(DateTime.Now.ToShortDateString().ToString(), "[^0-9]", "");
                time = DateTime.Now.TimeOfDay.ToString();
                time = time.Substring(0, 12).Replace(":", "_");

                using (Font myFont = new Font("Tw Cen MT", 12))
                {
                    string date = System.DateTime.Now.ToString("d");
                    Graphics gra = Graphics.FromImage(_frame.ToBitmap());
                    gra.DrawString(string.Concat(date, "-", time), myFont, Brushes.White, 10, 10);
                    _frame.ToBitmap().Save(img_path + "\\img\\" + daytime + "_" + time + "_" + pValue.Replace(" ", "").Substring(8, 8) + ".bmp");
                }
                ReturnValueCallback("C1 02 02 08 44 4F 4E 45");

            }
        }

        void funlock(bool _cmd)
        {
            bt_startcapture.Enabled = _cmd;
            cb_SelectCamera.Enabled = _cmd;
        }
        public cccSet()
        {

            InitializeComponent();
            DoubleBufferOn();
            Appset();
            _frame = new Mat();
            _grayFrame = new Mat();

            cb_SelectCamera.SelectedIndex = 0;
            selectCamindex = 0;

            form1 = new Form1();
            if (form1.DialogResult == DialogResult.OK)
            {
                //線上程中開啟主窗體
                Application.Run(new Form1());
            }
            tb_savepath.Text = img_path;


        }
        private void cccSet_Load(object sender, EventArgs e)
        {

        }
        private void Appset()
        {
            string path = System.Environment.CurrentDirectory;
            if (Directory.Exists(path + "\\img") == false) Directory.CreateDirectory(path + "\\img");

            cb_SelectCamera.Items.Clear();
            //check how many camera device
            devices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);

            camindex = 0;
            //accroding how mant device to create array number.
            Array.Resize(ref _capture, devices.Length);

            //accroding how mant device to create "new Capture(camindex)".
            foreach (DsDevice device in devices)
            {
                cb_SelectCamera.Items.Add(string.Concat("Camera ", camindex));
                _capture[camindex] = new VideoCapture(camindex);
                _capture[camindex].ImageGrabbed += ProcessFrame;

                camindex++;
            }


        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            if (_capture[selectCamindex] != null && _capture[selectCamindex].Ptr != IntPtr.Zero)
            {
                _capture[selectCamindex].Retrieve(_frame, 0);

                CvInvoke.CvtColor(_frame, _grayFrame, ColorConversion.Bgr2Gray);

                try
                {
                    ccc.Image = _frame;
                }
                catch (Exception exception)
                {
                        MessageBox.Show(exception.Message);
                    }
                GC.Collect();
                //grayscaleImageBox.Image = _grayFrame;
            }
        }
        private void bt_startcapture_Click(object sender, EventArgs e)
        {
            if (_capture[selectCamindex] != null)
            {
                if (!_captureInProgress)
                {
                    //stop the capture
                    _capture[selectCamindex].Start();
                    _captureInProgress = !_captureInProgress;
                }
                else
                {
                    //start the capture
                    _capture[selectCamindex].Pause();
                    _captureInProgress = !_captureInProgress;
                }
            }
            bt_startcapture.Text = _captureInProgress == true ? "Stop" : "Start";

        }
        private void ReleaseData()
        {
            if (_capture[selectCamindex] != null)
                _capture[selectCamindex].Dispose();
        }

        private void SelectResolution_TextChanged(object sender, EventArgs e)
        {
            //setting each camera property.
            try
            {
                //when the resolution be changed.
                string[] parts = cb_SelectResolution.Text.ToLower().Split('x');

                if (parts.Length == 2)
                {
                    int camWidth = int.Parse(parts[0]);
                    int camHeight = int.Parse(parts[1]);
                    if (this.camHeight != camHeight || this.camWidth != camWidth)
                    {
                        this.camWidth = camWidth;
                        this.camHeight = camHeight;
                    }
                }
              
                    _capture[selectCamindex].SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, camWidth);
                    _capture[selectCamindex].SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, camHeight);
                    _capture[selectCamindex].SetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps, 30);//設置每秒鐘的幀數
          
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cb_SelectCamera_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bt_startcapture.Text == "Stop")
                bt_startcapture.PerformClick();
            selectCamindex = cb_SelectCamera.SelectedIndex;


            if (bt_startcapture.Text == "Start")
                bt_startcapture.PerformClick();
        }

        private void bt_saveimage_Click(object sender, EventArgs e)
        {
            string daytime = System.Text.RegularExpressions.Regex.Replace(DateTime.Now.ToShortDateString().ToString(), "[^0-9]", "");
            time = DateTime.Now.TimeOfDay.ToString();
            time = time.Substring(0, 12).Replace(":", "_");

            using (Font myFont = new Font("Tw Cen MT", 12))
            {
                string date = System.DateTime.Now.ToString("d");
                Graphics gra = Graphics.FromImage(_frame.ToBitmap());
                gra.DrawString(string.Concat(date, "-", time), myFont, Brushes.White, 10, 10);

                if(!cb_archive.Checked)
                    _frame.ToBitmap().Save(img_path + "\\img\\" + daytime + time + ".bmp");
                else
                    VaryQualityLevel(_frame.ToBitmap(), System.IO.Path.Combine(img_path + "\\img\\", daytime + time+"_" + tb_archive.Text ), long.Parse(tb_archive.Text));
            }


            ReturnValueCallback("C1 02 02 08 44 4F 4E 45");
        }
        private static void VaryQualityLevel(Bitmap imgSourceFilePath, string outJpgFilePath, long qualityLevel = 90L)
        {

            using (Bitmap bmp1 = new Bitmap(imgSourceFilePath))
            {
                ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);

                System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;//給Compression無效

                EncoderParameters myEncoderParameters = new EncoderParameters(1);
                myEncoderParameters.Param[0] = new EncoderParameter(myEncoder, qualityLevel);
                bmp1.Save(outJpgFilePath + ".jpg", jpgEncoder, myEncoderParameters);
            }//end using

        }
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;

        }
        private void cccSet_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReturnValueCallback("C1 01 00 08 4F 46 46 00");
        }

        private void cccSet_FormClosing(object sender, FormClosingEventArgs e)
        {        
            camindex = 0;
            
            foreach (DsDevice device in devices)
            {
                _capture[camindex].Pause();
                _capture[camindex].Stop();
                camindex++;
            }
            ReturnValueCallback("C1 01 00 08 4F 46 46 00");
        }


        string time;
        private void ccc_Paint(object sender, PaintEventArgs e)
        {
            time = DateTime.Now.TimeOfDay.ToString();
            time = time.Substring(0, 12);
            using (Font myFont = new Font("Tw Cen MT", 12))
            {
                string date = System.DateTime.Now.ToString("d");

                //locate background mask at imagebox
                PointF pointF = new PointF(10, 10);
                SizeF sizeF = e.Graphics.MeasureString(string.Concat(date, "-", time), myFont);
                e.Graphics.FillRectangle(Brushes.Black, new RectangleF(pointF, sizeF));

                //locate time string at imagebox
                e.Graphics.DrawString(string.Concat(date, "-", time), myFont, Brushes.White, 10, 10);


            }
        }
    }
}
