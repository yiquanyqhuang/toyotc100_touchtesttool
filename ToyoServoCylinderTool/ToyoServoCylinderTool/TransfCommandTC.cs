﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace ToyoServoCylinderTool
{
    public class TransfCommandTC
    {
        //operation_mode
        //moving_coordinate
        //moving_speed
        //wait_time
        //force
        //beused
        Form1 frm = new Form1();
        public string stride_Mode;
        public string stride_distance;
        public string stride_delaytime;
        public string stride_speed;
        public string stride_force;
        public string stride_beused;
        public string stride_nextstride;
        public int stride_endindex;
        public int stride_index;
        public string loopindex;

        public byte[] GetModemsg()
        {
            //
            string MODEindex = indexconverthex(stride_index);
            //30 36 39 30 31 30 30 30 30 3x;
            byte[] bytesmsg = System.Text.Encoding.Default.GetBytes(stride_Mode.ToString());
            string result = System.Text.Encoding.UTF8.GetString(bytesmsg).PadLeft(2, '3');
            string modeStr_sub1 = string.Concat("30 36 39 ", MODEindex, "30 30 30 30 ");
            string modeStr = string.Concat(modeStr_sub1, result);
            modeStr = frm.ToyoASCII(modeStr);
            byte[] bytemsg = modeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;
        }
        public byte[] GetDistancemsg()
        {
            //
            string Disindex = indexconverthex(stride_index);
            //
            double distancedouble = double.Parse(stride_distance) * 100;
            string distance = distancedouble.ToString();
            distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
            distance = frm.ASCIIToHex(distance).Trim();
            string distancecommand;
            int distanceInt = (int)distancedouble;
            if (distanceInt >= 4096) distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 ", distance);
            else if (distanceInt < 4096 && distanceInt >= 256)
                distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 30 " + distance);
            else if (distanceInt < 256 && distanceInt >= 16)
                distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 30 30 " + distance);
            else
                distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance);
            string disStr = frm.ToyoASCII(distancecommand.Trim());
            byte[] bytemsg = disStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;
        }
        public byte[] GetDelaytime()
        {
            //
            string Delaytimeindex = indexconverthex(stride_index);
            //
            string delaytime = Convert.ToString(Int32.Parse(stride_delaytime), 16).ToUpper();
            delaytime = frm.ASCIIToHex(delaytime).Trim();
            string delaytimecommand;
            int delayInt = Int32.Parse(stride_delaytime);
            if (delayInt >= 4096)
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 " + delaytime);
            else if (delayInt < 4096 && delayInt >= 256)
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 30 " + delaytime);
            else if (delayInt < 256 && delayInt >= 16)
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 30 30 " + delaytime);
            else
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 30 30 30 " + delaytime);
            string delaytimeStr = frm.ToyoASCII(delaytimecommand.Trim());

            byte[] bytemsg = delaytimeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;

        }
        public byte[] GetSpeed()
        {
            //30 36 39 30 31 33 30 30 36 34
            string Speedindex = indexconverthex(stride_index);
            //
            string speedtime = Convert.ToString(Int32.Parse(stride_speed), 16).ToUpper();
            speedtime = frm.ASCIIToHex(speedtime).Trim();
            string speedtimecommand;
            int speedtimeInt = speedtime.Length;
            if (speedtimeInt <=2)
                speedtimecommand = string.Concat("30 36 39 ", Speedindex, "33 30 30 30 " + speedtime);
            else
                speedtimecommand = string.Concat("30 36 39 ", Speedindex, "33 30 30 " + speedtime);
            string speedtimeStr = frm.ToyoASCII(speedtimecommand.Trim());
            byte[] bytemsg = speedtimeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;

        }
        public byte[] GetNextStride()
        {
            //
            string NextStrideindex = indexconverthex(stride_index);
            //
            //30 36 39 30 31 30 30 30 30 3x;
            int IntStride_nextstride = int.Parse(stride_nextstride);
            string result = indexconverthex(IntStride_nextstride);
            //31 30
            string modeStr_sub1 = string.Concat("30 36 39 ", NextStrideindex, "44 30 30 ");

            string modeStr = string.Concat(modeStr_sub1, result.Trim());
            modeStr = frm.ToyoASCII(modeStr);
            byte[] bytemsg = modeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;

        }
        private string indexconverthex(int stride_index)
        {

            string strnum = stride_index.ToString();
            //
            //  int 1 = 30 31
            //  int 11 = 31 31
            //  int 127 = 3c 37
            //

            if (strnum.Length == 1)
            {
                string str = Convert.ToString(stride_index, 16).ToUpper().PadLeft(2, '3');
                str = string.Concat("30 ",str, " ");        //30 3x
                return str;
            }
            else if (strnum.Length == 2)
            {
                string str = frm.ASCIIToHex(stride_index.ToString());
                str = string.Concat( str.Trim(), " ");        //30 3x
                return str;
            }
            else
            {
                int prefix = int.Parse(strnum.Substring(0, 2));
                string str0 = Convert.ToString(prefix, 16).ToUpper().PadLeft(2, '3');
                string str1 = Convert.ToString(int.Parse(strnum.Substring(2, 1)), 16).ToUpper().PadLeft(2, '3');
                string str = string.Concat(str0, " ", str1, " ");        //30 3x
                return str;
                //MessageBox.Show("~" + str2 + "~");
            }

        }

    }
}
