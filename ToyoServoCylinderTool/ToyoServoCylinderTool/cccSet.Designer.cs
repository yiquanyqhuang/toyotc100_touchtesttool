﻿namespace ToyoServoCylinderTool
{
    partial class cccSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cccSet));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btNewTemplates = new System.Windows.Forms.ToolStripButton();
            this.btOpenTemplates = new System.Windows.Forms.ToolStripButton();
            this.btSaveTemplates = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.btCreateTemplate = new System.Windows.Forms.ToolStripButton();
            this.btAutoGenerate = new System.Windows.Forms.ToolStripButton();
            this.btTemplateEditor = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_SelectCamera = new System.Windows.Forms.ComboBox();
            this.cb_SelectResolution = new System.Windows.Forms.ComboBox();
            this.bt_savepath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_savepath = new System.Windows.Forms.TextBox();
            this.bt_saveimage = new System.Windows.Forms.Button();
            this.bt_startcapture = new System.Windows.Forms.Button();
            this.ccc = new Emgu.CV.UI.ImageBox();
            this.label25 = new System.Windows.Forms.Label();
            this.cb_archive = new System.Windows.Forms.CheckBox();
            this.tb_archive = new System.Windows.Forms.TextBox();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ccc)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNewTemplates,
            this.btOpenTemplates,
            this.btSaveTemplates,
            this.toolStripSeparator,
            this.btCreateTemplate,
            this.btAutoGenerate,
            this.btTemplateEditor});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(849, 27);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "ccc";
            // 
            // btNewTemplates
            // 
            this.btNewTemplates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btNewTemplates.Image = ((System.Drawing.Image)(resources.GetObject("btNewTemplates.Image")));
            this.btNewTemplates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNewTemplates.Name = "btNewTemplates";
            this.btNewTemplates.Size = new System.Drawing.Size(24, 24);
            this.btNewTemplates.Text = "New templates";
            // 
            // btOpenTemplates
            // 
            this.btOpenTemplates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btOpenTemplates.Image = ((System.Drawing.Image)(resources.GetObject("btOpenTemplates.Image")));
            this.btOpenTemplates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btOpenTemplates.Name = "btOpenTemplates";
            this.btOpenTemplates.Size = new System.Drawing.Size(24, 24);
            this.btOpenTemplates.Text = "Open templates";
            // 
            // btSaveTemplates
            // 
            this.btSaveTemplates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btSaveTemplates.Image = ((System.Drawing.Image)(resources.GetObject("btSaveTemplates.Image")));
            this.btSaveTemplates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSaveTemplates.Name = "btSaveTemplates";
            this.btSaveTemplates.Size = new System.Drawing.Size(24, 24);
            this.btSaveTemplates.Text = "Save templates";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // btCreateTemplate
            // 
            this.btCreateTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btCreateTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btCreateTemplate.Image")));
            this.btCreateTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btCreateTemplate.Name = "btCreateTemplate";
            this.btCreateTemplate.Size = new System.Drawing.Size(24, 24);
            this.btCreateTemplate.Text = "Create template";
            // 
            // btAutoGenerate
            // 
            this.btAutoGenerate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btAutoGenerate.Image = ((System.Drawing.Image)(resources.GetObject("btAutoGenerate.Image")));
            this.btAutoGenerate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAutoGenerate.Name = "btAutoGenerate";
            this.btAutoGenerate.Size = new System.Drawing.Size(24, 24);
            this.btAutoGenerate.Text = "Auto generate templates";
            // 
            // btTemplateEditor
            // 
            this.btTemplateEditor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btTemplateEditor.Image = ((System.Drawing.Image)(resources.GetObject("btTemplateEditor.Image")));
            this.btTemplateEditor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btTemplateEditor.Name = "btTemplateEditor";
            this.btTemplateEditor.Size = new System.Drawing.Size(24, 24);
            this.btTemplateEditor.Text = "Template viewer";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(849, 474);
            this.panel1.TabIndex = 10;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Panel1.Controls.Add(this.tb_archive);
            this.splitContainer1.Panel1.Controls.Add(this.cb_archive);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.cb_SelectCamera);
            this.splitContainer1.Panel1.Controls.Add(this.cb_SelectResolution);
            this.splitContainer1.Panel1.Controls.Add(this.bt_savepath);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.tb_savepath);
            this.splitContainer1.Panel1.Controls.Add(this.bt_saveimage);
            this.splitContainer1.Panel1.Controls.Add(this.bt_startcapture);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ccc);
            this.splitContainer1.Size = new System.Drawing.Size(800, 468);
            this.splitContainer1.SplitterDistance = 162;
            this.splitContainer1.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(9, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Camera:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(9, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "Resolution:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(9, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Start/Stop Capture";
            // 
            // cb_SelectCamera
            // 
            this.cb_SelectCamera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_SelectCamera.Font = new System.Drawing.Font("微軟正黑體", 9F);
            this.cb_SelectCamera.FormattingEnabled = true;
            this.cb_SelectCamera.Location = new System.Drawing.Point(9, 82);
            this.cb_SelectCamera.Name = "cb_SelectCamera";
            this.cb_SelectCamera.Size = new System.Drawing.Size(98, 24);
            this.cb_SelectCamera.TabIndex = 12;
            this.cb_SelectCamera.Text = "Camera";
            this.cb_SelectCamera.SelectedIndexChanged += new System.EventHandler(this.cb_SelectCamera_SelectedIndexChanged);
            // 
            // cb_SelectResolution
            // 
            this.cb_SelectResolution.Enabled = false;
            this.cb_SelectResolution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_SelectResolution.Font = new System.Drawing.Font("微軟正黑體", 9F);
            this.cb_SelectResolution.FormattingEnabled = true;
            this.cb_SelectResolution.Items.AddRange(new object[] {
            "160x120",
            "176x144",
            "320x240",
            "352x288",
            "640x480",
            "1280x720"});
            this.cb_SelectResolution.Location = new System.Drawing.Point(9, 130);
            this.cb_SelectResolution.Name = "cb_SelectResolution";
            this.cb_SelectResolution.Size = new System.Drawing.Size(98, 24);
            this.cb_SelectResolution.TabIndex = 11;
            this.cb_SelectResolution.Text = "640x480";
            this.cb_SelectResolution.TextChanged += new System.EventHandler(this.SelectResolution_TextChanged);
            // 
            // bt_savepath
            // 
            this.bt_savepath.Enabled = false;
            this.bt_savepath.Font = new System.Drawing.Font("微軟正黑體", 9F);
            this.bt_savepath.Location = new System.Drawing.Point(9, 178);
            this.bt_savepath.Name = "bt_savepath";
            this.bt_savepath.Size = new System.Drawing.Size(75, 23);
            this.bt_savepath.TabIndex = 4;
            this.bt_savepath.Text = "path";
            this.bt_savepath.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(9, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "file name";
            // 
            // tb_savepath
            // 
            this.tb_savepath.Enabled = false;
            this.tb_savepath.Font = new System.Drawing.Font("微軟正黑體", 9F);
            this.tb_savepath.Location = new System.Drawing.Point(9, 205);
            this.tb_savepath.Name = "tb_savepath";
            this.tb_savepath.Size = new System.Drawing.Size(137, 23);
            this.tb_savepath.TabIndex = 2;
            // 
            // bt_saveimage
            // 
            this.bt_saveimage.Font = new System.Drawing.Font("微軟正黑體", 9F);
            this.bt_saveimage.Location = new System.Drawing.Point(9, 232);
            this.bt_saveimage.Name = "bt_saveimage";
            this.bt_saveimage.Size = new System.Drawing.Size(75, 23);
            this.bt_saveimage.TabIndex = 1;
            this.bt_saveimage.Text = "Save";
            this.bt_saveimage.UseVisualStyleBackColor = true;
            this.bt_saveimage.Click += new System.EventHandler(this.bt_saveimage_Click);
            // 
            // bt_startcapture
            // 
            this.bt_startcapture.Font = new System.Drawing.Font("微軟正黑體", 9F);
            this.bt_startcapture.Location = new System.Drawing.Point(9, 35);
            this.bt_startcapture.Name = "bt_startcapture";
            this.bt_startcapture.Size = new System.Drawing.Size(75, 23);
            this.bt_startcapture.TabIndex = 0;
            this.bt_startcapture.Text = "Start";
            this.bt_startcapture.UseVisualStyleBackColor = true;
            this.bt_startcapture.Click += new System.EventHandler(this.bt_startcapture_Click);
            // 
            // ccc
            // 
            this.ccc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ccc.Location = new System.Drawing.Point(0, 0);
            this.ccc.Name = "ccc";
            this.ccc.Size = new System.Drawing.Size(634, 468);
            this.ccc.TabIndex = 2;
            this.ccc.TabStop = false;
            this.ccc.Paint += new System.Windows.Forms.PaintEventHandler(this.ccc_Paint);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label25.ForeColor = System.Drawing.Color.Gray;
            this.label25.Location = new System.Drawing.Point(749, 507);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 16);
            this.label25.TabIndex = 77;
            this.label25.Text = "cccSet 00.01.01";
            // 
            // cb_archive
            // 
            this.cb_archive.AutoSize = true;
            this.cb_archive.Location = new System.Drawing.Point(12, 262);
            this.cb_archive.Name = "cb_archive";
            this.cb_archive.Size = new System.Drawing.Size(58, 16);
            this.cb_archive.TabIndex = 16;
            this.cb_archive.Text = "archive";
            this.cb_archive.UseVisualStyleBackColor = true;
            // 
            // tb_archive
            // 
            this.tb_archive.Location = new System.Drawing.Point(75, 260);
            this.tb_archive.Name = "tb_archive";
            this.tb_archive.Size = new System.Drawing.Size(55, 22);
            this.tb_archive.TabIndex = 17;
            this.tb_archive.Text = "80";
            // 
            // cccSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 532);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "cccSet";
            this.Text = "cccSet";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.cccSet_FormClosing);
            this.Load += new System.EventHandler(this.cccSet_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ccc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btNewTemplates;
        private System.Windows.Forms.ToolStripButton btOpenTemplates;
        private System.Windows.Forms.ToolStripButton btSaveTemplates;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton btCreateTemplate;
        private System.Windows.Forms.ToolStripButton btAutoGenerate;
        private System.Windows.Forms.ToolStripButton btTemplateEditor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox cb_SelectCamera;
        private System.Windows.Forms.ComboBox cb_SelectResolution;
        private System.Windows.Forms.Button bt_savepath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_savepath;
        public System.Windows.Forms.Button bt_saveimage;
        private System.Windows.Forms.Button bt_startcapture;
        private Emgu.CV.UI.ImageBox ccc;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_archive;
        private System.Windows.Forms.CheckBox cb_archive;
    }
}