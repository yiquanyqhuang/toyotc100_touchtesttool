﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyoServoCylinderTool
{
    class YQ_protocal
    {
        /* 1" */
        ///
        /// C0"
        /// main -> slave
        ///
        /// C1"
        /// slave -> main



        /* 2" */
        ///
        /// 01 form operation
        ///
        ///
        ///
        /// 02 pic operation
        ///     /* 3" */
        ///     01 load img
        ///
        ///     02 save img



        /// form status
        /// C0 01 00 08 4D 4F 44 45 -> ON
        ///
        /// form OPEN
        /// C1 01 00 08 4F 4E 00 00 -> ON
        ///
        /// form disable
        /// C0 02 01 08 73 74 6F 70
        ///
        /// form able
        /// C0 02 01 08 61 62 6C 65
        ///


        /// form CLOSE
        /// C1 01 00 08 4F 46 46 00 -> OFF

        /// img save
        /// C0 02 02 08 xx xx xx xx -> SAVE
        /// ex.
        /// touch count =123456 (round time)
        /// C0 02 02 08 00 12 34 56

        /// img  save img DONE
        /// C1 02 02 08 44 4F 4E 45 -> DONE
        ///

        ///ref
        ///https://dotblogs.com.tw/joysdw12/2013/06/21/delegate-winfom
    }
}
