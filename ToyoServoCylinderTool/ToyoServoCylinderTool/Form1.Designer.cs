﻿namespace ToyoServoCylinderTool
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_distance = new System.Windows.Forms.TextBox();
            this.textBox_Speed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_delaytime = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_port = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_com = new System.Windows.Forms.ComboBox();
            this.button_buttonOpenCloseCom = new System.Windows.Forms.Button();
            this.label_COMconnect = new System.Windows.Forms.Label();
            this.button_SearchCOM = new System.Windows.Forms.Button();
            this.button_SendCommand = new System.Windows.Forms.Button();
            this.button_SearchCOM2 = new System.Windows.Forms.Button();
            this.label_COMconnect2 = new System.Windows.Forms.Label();
            this.button_buttonOpenCloseCom2 = new System.Windows.Forms.Button();
            this.comboBox_com2 = new System.Windows.Forms.ComboBox();
            this.rtxtDataArea = new System.Windows.Forms.RichTextBox();
            this.textBox_time = new System.Windows.Forms.TextBox();
            this.label_count = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboBox_com3 = new System.Windows.Forms.ComboBox();
            this.button_buttonOpenCloseCom3 = new System.Windows.Forms.Button();
            this.label_COMconnect3 = new System.Windows.Forms.Label();
            this.button_SearchCOM3 = new System.Windows.Forms.Button();
            this.ToyoOnOff = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button_emergencystop = new System.Windows.Forms.Button();
            this.button_set = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button_autostart = new System.Windows.Forms.Button();
            this.textBox_weight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_enddistance = new System.Windows.Forms.TextBox();
            this.button_Normaltest = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_startdistance = new System.Windows.Forms.TextBox();
            this.button_calibration = new System.Windows.Forms.Button();
            this.button_Unlock = new System.Windows.Forms.Button();
            this.button_ZeroDistance = new System.Windows.Forms.Button();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.timer_TimeDetect = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.dgvTemplates = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bt_RunScheduler = new System.Windows.Forms.Button();
            this.bt_Exportini = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.pBar1 = new System.Windows.Forms.ProgressBar();
            this.bt_savelog = new System.Windows.Forms.Button();
            this.tabC_ = new System.Windows.Forms.TabControl();
            this.tabP_PushScript = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Recordtype_no = new System.Windows.Forms.CheckBox();
            this.Recordtype_yes = new System.Windows.Forms.CheckBox();
            this.tb_CorrectionValue = new System.Windows.Forms.TextBox();
            this.tabP_Monitor = new System.Windows.Forms.TabPage();
            this.bt_CleardgvTables = new System.Windows.Forms.Button();
            this.dgvTables = new System.Windows.Forms.DataGridView();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabP_WeightTest = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.tb_freq_saveimgimg = new System.Windows.Forms.TextBox();
            this.cb_wt_detectpress = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tb_freq_saveimgcsv = new System.Windows.Forms.TextBox();
            this.bt_cccsetform = new System.Windows.Forms.Button();
            this.bt_ClearWTlog = new System.Windows.Forms.Button();
            this.bt_wt_savelog = new System.Windows.Forms.Button();
            this.dgvTables_wt = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bt_wt_lock = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.bt_WT_calibration = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tb_pushtimes = new System.Windows.Forms.TextBox();
            this.tb_templeWeight = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label16 = new System.Windows.Forms.Label();
            this.nUD_curStress = new System.Windows.Forms.NumericUpDown();
            this.tb_lenInterval = new System.Windows.Forms.TextBox();
            this.bt_Weightdetect = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Stride_timer = new System.Windows.Forms.Timer(this.components);
            this.bt_ForceQuit = new System.Windows.Forms.Button();
            this.RunScheduler_timer = new System.Windows.Forms.Timer(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.trackerbar_time = new System.Windows.Forms.Timer(this.components);
            this.label24 = new System.Windows.Forms.Label();
            this.time_cur_weight = new System.Windows.Forms.Timer(this.components);
            this.trace_report_time = new System.Windows.Forms.Timer(this.components);
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.idle_time = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemplates)).BeginInit();
            this.tabC_.SuspendLayout();
            this.tabP_PushScript.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabP_Monitor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTables)).BeginInit();
            this.tabP_WeightTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTables_wt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_curStress)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1112, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Create";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.numericUpDown1.Location = new System.Drawing.Point(33, 136);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(100, 22);
            this.numericUpDown1.TabIndex = 9;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(33, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "Station Number";
            // 
            // textBox_distance
            // 
            this.textBox_distance.Enabled = false;
            this.textBox_distance.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_distance.Location = new System.Drawing.Point(35, 39);
            this.textBox_distance.Name = "textBox_distance";
            this.textBox_distance.Size = new System.Drawing.Size(100, 22);
            this.textBox_distance.TabIndex = 11;
            this.textBox_distance.Text = "5";
            this.textBox_distance.TextChanged += new System.EventHandler(this.textBox_distance_TextChanged);
            // 
            // textBox_Speed
            // 
            this.textBox_Speed.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_Speed.Location = new System.Drawing.Point(201, 136);
            this.textBox_Speed.Name = "textBox_Speed";
            this.textBox_Speed.Size = new System.Drawing.Size(100, 22);
            this.textBox_Speed.TabIndex = 12;
            this.textBox_Speed.Text = "100";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(33, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "Absolute_distance(0~50mm)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(199, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "Speed(0~100%)";
            // 
            // textBox_delaytime
            // 
            this.textBox_delaytime.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_delaytime.Location = new System.Drawing.Point(33, 187);
            this.textBox_delaytime.Name = "textBox_delaytime";
            this.textBox_delaytime.Size = new System.Drawing.Size(100, 22);
            this.textBox_delaytime.TabIndex = 16;
            this.textBox_delaytime.Text = "500";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(31, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "Delay Time";
            // 
            // textBox_port
            // 
            this.textBox_port.Location = new System.Drawing.Point(1083, 39);
            this.textBox_port.Name = "textBox_port";
            this.textBox_port.Size = new System.Drawing.Size(100, 22);
            this.textBox_port.TabIndex = 18;
            this.textBox_port.Text = "A";
            this.textBox_port.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1082, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 12);
            this.label6.TabIndex = 19;
            this.label6.Text = "Port";
            this.label6.Visible = false;
            // 
            // comboBox_com
            // 
            this.comboBox_com.Font = new System.Drawing.Font("新細明體", 9F);
            this.comboBox_com.FormattingEnabled = true;
            this.comboBox_com.Location = new System.Drawing.Point(6, 21);
            this.comboBox_com.Name = "comboBox_com";
            this.comboBox_com.Size = new System.Drawing.Size(121, 20);
            this.comboBox_com.TabIndex = 22;
            // 
            // button_buttonOpenCloseCom
            // 
            this.button_buttonOpenCloseCom.Font = new System.Drawing.Font("新細明體", 9F);
            this.button_buttonOpenCloseCom.Location = new System.Drawing.Point(6, 54);
            this.button_buttonOpenCloseCom.Name = "button_buttonOpenCloseCom";
            this.button_buttonOpenCloseCom.Size = new System.Drawing.Size(75, 23);
            this.button_buttonOpenCloseCom.TabIndex = 23;
            this.button_buttonOpenCloseCom.Text = "Open COM";
            this.button_buttonOpenCloseCom.UseVisualStyleBackColor = true;
            this.button_buttonOpenCloseCom.Click += new System.EventHandler(this.button_buttonOpenCloseCom_Click);
            // 
            // label_COMconnect
            // 
            this.label_COMconnect.AutoSize = true;
            this.label_COMconnect.Font = new System.Drawing.Font("新細明體", 9F);
            this.label_COMconnect.Location = new System.Drawing.Point(87, 59);
            this.label_COMconnect.Name = "label_COMconnect";
            this.label_COMconnect.Size = new System.Drawing.Size(90, 12);
            this.label_COMconnect.TabIndex = 24;
            this.label_COMconnect.Text = "Please Open COM";
            // 
            // button_SearchCOM
            // 
            this.button_SearchCOM.Font = new System.Drawing.Font("新細明體", 9F);
            this.button_SearchCOM.Location = new System.Drawing.Point(134, 21);
            this.button_SearchCOM.Name = "button_SearchCOM";
            this.button_SearchCOM.Size = new System.Drawing.Size(75, 23);
            this.button_SearchCOM.TabIndex = 25;
            this.button_SearchCOM.Text = "SearchCOM";
            this.button_SearchCOM.UseVisualStyleBackColor = true;
            this.button_SearchCOM.Click += new System.EventHandler(this.button_SearchCOM_Click);
            // 
            // button_SendCommand
            // 
            this.button_SendCommand.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_SendCommand.Location = new System.Drawing.Point(1193, 39);
            this.button_SendCommand.Name = "button_SendCommand";
            this.button_SendCommand.Size = new System.Drawing.Size(95, 23);
            this.button_SendCommand.TabIndex = 26;
            this.button_SendCommand.Text = "START";
            this.button_SendCommand.UseVisualStyleBackColor = true;
            this.button_SendCommand.Visible = false;
            this.button_SendCommand.Click += new System.EventHandler(this.button_SendCommand_Click);
            // 
            // button_SearchCOM2
            // 
            this.button_SearchCOM2.Font = new System.Drawing.Font("新細明體", 9F);
            this.button_SearchCOM2.Location = new System.Drawing.Point(134, 21);
            this.button_SearchCOM2.Name = "button_SearchCOM2";
            this.button_SearchCOM2.Size = new System.Drawing.Size(75, 23);
            this.button_SearchCOM2.TabIndex = 30;
            this.button_SearchCOM2.Text = "SearchCOM";
            this.button_SearchCOM2.UseVisualStyleBackColor = true;
            this.button_SearchCOM2.Click += new System.EventHandler(this.button_SearchCOM2_Click);
            // 
            // label_COMconnect2
            // 
            this.label_COMconnect2.AutoSize = true;
            this.label_COMconnect2.Font = new System.Drawing.Font("新細明體", 9F);
            this.label_COMconnect2.Location = new System.Drawing.Point(87, 59);
            this.label_COMconnect2.Name = "label_COMconnect2";
            this.label_COMconnect2.Size = new System.Drawing.Size(90, 12);
            this.label_COMconnect2.TabIndex = 29;
            this.label_COMconnect2.Text = "Please Open COM";
            // 
            // button_buttonOpenCloseCom2
            // 
            this.button_buttonOpenCloseCom2.Font = new System.Drawing.Font("新細明體", 9F);
            this.button_buttonOpenCloseCom2.Location = new System.Drawing.Point(6, 54);
            this.button_buttonOpenCloseCom2.Name = "button_buttonOpenCloseCom2";
            this.button_buttonOpenCloseCom2.Size = new System.Drawing.Size(75, 23);
            this.button_buttonOpenCloseCom2.TabIndex = 28;
            this.button_buttonOpenCloseCom2.Text = "Open COM";
            this.button_buttonOpenCloseCom2.UseVisualStyleBackColor = true;
            this.button_buttonOpenCloseCom2.Click += new System.EventHandler(this.button_buttonOpenCloseCom2_Click);
            // 
            // comboBox_com2
            // 
            this.comboBox_com2.Font = new System.Drawing.Font("新細明體", 9F);
            this.comboBox_com2.FormattingEnabled = true;
            this.comboBox_com2.Location = new System.Drawing.Point(6, 21);
            this.comboBox_com2.Name = "comboBox_com2";
            this.comboBox_com2.Size = new System.Drawing.Size(121, 20);
            this.comboBox_com2.TabIndex = 27;
            // 
            // rtxtDataArea
            // 
            this.rtxtDataArea.Location = new System.Drawing.Point(1123, 67);
            this.rtxtDataArea.Name = "rtxtDataArea";
            this.rtxtDataArea.Size = new System.Drawing.Size(137, 198);
            this.rtxtDataArea.TabIndex = 33;
            this.rtxtDataArea.Text = "";
            this.rtxtDataArea.Visible = false;
            // 
            // textBox_time
            // 
            this.textBox_time.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_time.Location = new System.Drawing.Point(201, 187);
            this.textBox_time.Name = "textBox_time";
            this.textBox_time.Size = new System.Drawing.Size(100, 22);
            this.textBox_time.TabIndex = 34;
            this.textBox_time.Text = "7";
            // 
            // label_count
            // 
            this.label_count.AutoSize = true;
            this.label_count.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_count.ForeColor = System.Drawing.Color.Red;
            this.label_count.Location = new System.Drawing.Point(7, 399);
            this.label_count.Name = "label_count";
            this.label_count.Size = new System.Drawing.Size(86, 35);
            this.label_count.TabIndex = 36;
            this.label_count.Text = "STOP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(201, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 12);
            this.label4.TabIndex = 37;
            this.label4.Text = "Push time";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox_com);
            this.groupBox1.Controls.Add(this.button_buttonOpenCloseCom);
            this.groupBox1.Controls.Add(this.label_COMconnect);
            this.groupBox1.Controls.Add(this.button_SearchCOM);
            this.groupBox1.Font = new System.Drawing.Font("新細明體", 11F);
            this.groupBox1.Location = new System.Drawing.Point(14, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 89);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Toyo Port";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox_com2);
            this.groupBox2.Controls.Add(this.button_buttonOpenCloseCom2);
            this.groupBox2.Controls.Add(this.label_COMconnect2);
            this.groupBox2.Controls.Add(this.button_SearchCOM2);
            this.groupBox2.Font = new System.Drawing.Font("新細明體", 11F);
            this.groupBox2.Location = new System.Drawing.Point(14, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(215, 91);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Arduino Port";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox8);
            this.groupBox3.Controls.Add(this.ToyoOnOff);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.label_count);
            this.groupBox3.Font = new System.Drawing.Font("新細明體", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox3.Location = new System.Drawing.Point(12, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(242, 461);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "COM Port Connect";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.comboBox_com3);
            this.groupBox8.Controls.Add(this.button_buttonOpenCloseCom3);
            this.groupBox8.Controls.Add(this.label_COMconnect3);
            this.groupBox8.Controls.Add(this.button_SearchCOM3);
            this.groupBox8.Font = new System.Drawing.Font("新細明體", 11F);
            this.groupBox8.Location = new System.Drawing.Point(14, 248);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(215, 91);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "DB";
            // 
            // comboBox_com3
            // 
            this.comboBox_com3.Font = new System.Drawing.Font("新細明體", 9F);
            this.comboBox_com3.FormattingEnabled = true;
            this.comboBox_com3.Location = new System.Drawing.Point(6, 21);
            this.comboBox_com3.Name = "comboBox_com3";
            this.comboBox_com3.Size = new System.Drawing.Size(121, 20);
            this.comboBox_com3.TabIndex = 27;
            // 
            // button_buttonOpenCloseCom3
            // 
            this.button_buttonOpenCloseCom3.Font = new System.Drawing.Font("新細明體", 9F);
            this.button_buttonOpenCloseCom3.Location = new System.Drawing.Point(6, 54);
            this.button_buttonOpenCloseCom3.Name = "button_buttonOpenCloseCom3";
            this.button_buttonOpenCloseCom3.Size = new System.Drawing.Size(75, 23);
            this.button_buttonOpenCloseCom3.TabIndex = 28;
            this.button_buttonOpenCloseCom3.Text = "Open COM";
            this.button_buttonOpenCloseCom3.UseVisualStyleBackColor = true;
            this.button_buttonOpenCloseCom3.Click += new System.EventHandler(this.button_buttonOpenCloseCom3_Click);
            // 
            // label_COMconnect3
            // 
            this.label_COMconnect3.AutoSize = true;
            this.label_COMconnect3.Font = new System.Drawing.Font("新細明體", 9F);
            this.label_COMconnect3.Location = new System.Drawing.Point(87, 59);
            this.label_COMconnect3.Name = "label_COMconnect3";
            this.label_COMconnect3.Size = new System.Drawing.Size(90, 12);
            this.label_COMconnect3.TabIndex = 29;
            this.label_COMconnect3.Text = "Please Open COM";
            // 
            // button_SearchCOM3
            // 
            this.button_SearchCOM3.Font = new System.Drawing.Font("新細明體", 9F);
            this.button_SearchCOM3.Location = new System.Drawing.Point(134, 21);
            this.button_SearchCOM3.Name = "button_SearchCOM3";
            this.button_SearchCOM3.Size = new System.Drawing.Size(75, 23);
            this.button_SearchCOM3.TabIndex = 30;
            this.button_SearchCOM3.Text = "SearchCOM";
            this.button_SearchCOM3.UseVisualStyleBackColor = true;
            this.button_SearchCOM3.Click += new System.EventHandler(this.button_SearchCOM3_Click);
            // 
            // ToyoOnOff
            // 
            this.ToyoOnOff.Location = new System.Drawing.Point(14, 24);
            this.ToyoOnOff.Name = "ToyoOnOff";
            this.ToyoOnOff.Size = new System.Drawing.Size(215, 26);
            this.ToyoOnOff.TabIndex = 41;
            this.ToyoOnOff.Text = "Power";
            this.ToyoOnOff.UseVisualStyleBackColor = true;
            this.ToyoOnOff.Click += new System.EventHandler(this.ToyoOnOff_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.ForeColor = System.Drawing.Color.Green;
            this.label8.Location = new System.Drawing.Point(8, 339);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 35);
            this.label8.TabIndex = 40;
            this.label8.Text = "          ";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(611, 249);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox3.Size = new System.Drawing.Size(523, 135);
            this.textBox3.TabIndex = 41;
            // 
            // button_emergencystop
            // 
            this.button_emergencystop.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_emergencystop.ForeColor = System.Drawing.Color.Black;
            this.button_emergencystop.Location = new System.Drawing.Point(23, 382);
            this.button_emergencystop.Name = "button_emergencystop";
            this.button_emergencystop.Size = new System.Drawing.Size(95, 23);
            this.button_emergencystop.TabIndex = 40;
            this.button_emergencystop.Text = "Emergency Stop";
            this.button_emergencystop.UseVisualStyleBackColor = true;
            this.button_emergencystop.Click += new System.EventHandler(this.button_emergencystop_Click);
            // 
            // button_set
            // 
            this.button_set.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_set.Location = new System.Drawing.Point(1193, 10);
            this.button_set.Name = "button_set";
            this.button_set.Size = new System.Drawing.Size(95, 23);
            this.button_set.TabIndex = 41;
            this.button_set.Text = "Setting Toyo";
            this.button_set.UseVisualStyleBackColor = true;
            this.button_set.Visible = false;
            this.button_set.Click += new System.EventHandler(this.button_set_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listBox4);
            this.groupBox4.Controls.Add(this.listBox2);
            this.groupBox4.Controls.Add(this.listBox3);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.button_calibration);
            this.groupBox4.Controls.Add(this.button_Unlock);
            this.groupBox4.Controls.Add(this.button_ZeroDistance);
            this.groupBox4.Controls.Add(this.hScrollBar1);
            this.groupBox4.Controls.Add(this.button_emergencystop);
            this.groupBox4.Controls.Add(this.textBox_delaytime);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.numericUpDown1);
            this.groupBox4.Controls.Add(this.textBox_distance);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.textBox_Speed);
            this.groupBox4.Controls.Add(this.textBox_time);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Font = new System.Drawing.Font("新細明體", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox4.Location = new System.Drawing.Point(260, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(345, 415);
            this.groupBox4.TabIndex = 42;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Auto Push";
            // 
            // listBox4
            // 
            this.listBox4.FormattingEnabled = true;
            this.listBox4.ItemHeight = 17;
            this.listBox4.Location = new System.Drawing.Point(102, 115);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(88, 225);
            this.listBox4.TabIndex = 61;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 17;
            this.listBox2.Location = new System.Drawing.Point(188, 74);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(151, 225);
            this.listBox2.TabIndex = 60;
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 17;
            this.listBox3.Location = new System.Drawing.Point(13, 24);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(151, 225);
            this.listBox3.TabIndex = 59;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button_autostart);
            this.groupBox6.Controls.Add(this.textBox_weight);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox6.Location = new System.Drawing.Point(11, 296);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(322, 72);
            this.groupBox6.TabIndex = 56;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Specified weight test";
            // 
            // button_autostart
            // 
            this.button_autostart.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_autostart.Location = new System.Drawing.Point(221, 40);
            this.button_autostart.Name = "button_autostart";
            this.button_autostart.Size = new System.Drawing.Size(95, 23);
            this.button_autostart.TabIndex = 46;
            this.button_autostart.Text = "Weight test";
            this.button_autostart.UseVisualStyleBackColor = true;
            this.button_autostart.Click += new System.EventHandler(this.button_autostart_Click);
            // 
            // textBox_weight
            // 
            this.textBox_weight.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_weight.Location = new System.Drawing.Point(12, 41);
            this.textBox_weight.Name = "textBox_weight";
            this.textBox_weight.Size = new System.Drawing.Size(100, 22);
            this.textBox_weight.TabIndex = 45;
            this.textBox_weight.Text = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(12, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 48;
            this.label7.Text = "Weight(kg)";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.textBox_enddistance);
            this.groupBox5.Controls.Add(this.button_Normaltest);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.textBox_startdistance);
            this.groupBox5.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox5.Location = new System.Drawing.Point(13, 218);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(322, 72);
            this.groupBox5.TabIndex = 55;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Specified distance test";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(8, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 12);
            this.label11.TabIndex = 50;
            this.label11.Text = "Start distance";
            // 
            // textBox_enddistance
            // 
            this.textBox_enddistance.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_enddistance.Location = new System.Drawing.Point(116, 43);
            this.textBox_enddistance.Name = "textBox_enddistance";
            this.textBox_enddistance.Size = new System.Drawing.Size(100, 22);
            this.textBox_enddistance.TabIndex = 51;
            this.textBox_enddistance.Text = "20";
            // 
            // button_Normaltest
            // 
            this.button_Normaltest.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Normaltest.Location = new System.Drawing.Point(219, 41);
            this.button_Normaltest.Name = "button_Normaltest";
            this.button_Normaltest.Size = new System.Drawing.Size(95, 23);
            this.button_Normaltest.TabIndex = 53;
            this.button_Normaltest.Text = "Distance test";
            this.button_Normaltest.UseVisualStyleBackColor = true;
            this.button_Normaltest.Click += new System.EventHandler(this.button_Normaltest_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(116, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 12);
            this.label10.TabIndex = 52;
            this.label10.Text = "End distance";
            // 
            // textBox_startdistance
            // 
            this.textBox_startdistance.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_startdistance.Location = new System.Drawing.Point(10, 43);
            this.textBox_startdistance.Name = "textBox_startdistance";
            this.textBox_startdistance.Size = new System.Drawing.Size(100, 22);
            this.textBox_startdistance.TabIndex = 49;
            this.textBox_startdistance.Text = "0";
            // 
            // button_calibration
            // 
            this.button_calibration.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_calibration.Location = new System.Drawing.Point(232, 382);
            this.button_calibration.Name = "button_calibration";
            this.button_calibration.Size = new System.Drawing.Size(95, 23);
            this.button_calibration.TabIndex = 54;
            this.button_calibration.Text = "Get weight";
            this.button_calibration.UseVisualStyleBackColor = true;
            this.button_calibration.Click += new System.EventHandler(this.button_calibration_Click);
            // 
            // button_Unlock
            // 
            this.button_Unlock.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Unlock.Location = new System.Drawing.Point(127, 382);
            this.button_Unlock.Name = "button_Unlock";
            this.button_Unlock.Size = new System.Drawing.Size(95, 23);
            this.button_Unlock.TabIndex = 47;
            this.button_Unlock.Text = "Stop Unlock";
            this.button_Unlock.UseVisualStyleBackColor = true;
            this.button_Unlock.Click += new System.EventHandler(this.button_Unlock_Click);
            // 
            // button_ZeroDistance
            // 
            this.button_ZeroDistance.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_ZeroDistance.Location = new System.Drawing.Point(222, 37);
            this.button_ZeroDistance.Name = "button_ZeroDistance";
            this.button_ZeroDistance.Size = new System.Drawing.Size(95, 23);
            this.button_ZeroDistance.TabIndex = 44;
            this.button_ZeroDistance.Text = "CalZero";
            this.button_ZeroDistance.UseVisualStyleBackColor = true;
            this.button_ZeroDistance.Click += new System.EventHandler(this.button_ZeroDistance_Click);
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.LargeChange = 1;
            this.hScrollBar1.Location = new System.Drawing.Point(19, 64);
            this.hScrollBar1.Maximum = 5000;
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(298, 36);
            this.hScrollBar1.TabIndex = 43;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(611, 91);
            this.listBox1.Name = "listBox1";
            this.listBox1.ScrollAlwaysVisible = true;
            this.listBox1.Size = new System.Drawing.Size(689, 64);
            this.listBox1.TabIndex = 59;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // timer_TimeDetect
            // 
            this.timer_TimeDetect.Interval = 1000;
            this.timer_TimeDetect.Tick += new System.EventHandler(this.timer_TimeDetect_Tick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(763, 62);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 43;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(696, 161);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 45;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(696, 190);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(389, 21);
            this.textBox1.TabIndex = 46;
            this.textBox1.Text = "30 33 31 30 30 38 30 30 30 32";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(696, 216);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(389, 22);
            this.textBox2.TabIndex = 47;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(696, 244);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(65, 23);
            this.button4.TabIndex = 48;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dgvTemplates
            // 
            this.dgvTemplates.AllowUserToAddRows = false;
            this.dgvTemplates.AllowUserToOrderColumns = true;
            this.dgvTemplates.AllowUserToResizeColumns = false;
            this.dgvTemplates.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTemplates.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTemplates.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTemplates.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvTemplates.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTemplates.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column10});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTemplates.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTemplates.EnableHeadersVisualStyles = false;
            this.dgvTemplates.Location = new System.Drawing.Point(3, 6);
            this.dgvTemplates.MultiSelect = false;
            this.dgvTemplates.Name = "dgvTemplates";
            this.dgvTemplates.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTemplates.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvTemplates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTemplates.Size = new System.Drawing.Size(681, 356);
            this.dgvTemplates.TabIndex = 49;
            this.dgvTemplates.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTemplates_CellEndEdit);
            this.dgvTemplates.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dgvTemplates_CellValueNeeded);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.FillWeight = 1F;
            this.Column1.HeaderText = "id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 37;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 97F;
            this.Column2.HeaderText = "運算模式";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.FillWeight = 97F;
            this.Column3.HeaderText = "移動座標(0~50)";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.FillWeight = 97.07006F;
            this.Column4.HeaderText = "移動速度(0~100 %)";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.FillWeight = 97F;
            this.Column5.HeaderText = "等待(0~30000)";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.FillWeight = 97F;
            this.Column6.HeaderText = "Force";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.FillWeight = 114.6497F;
            this.Column7.HeaderText = "啟用";
            this.Column7.Name = "Column7";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "記錄點";
            this.Column10.Name = "Column10";
            // 
            // bt_RunScheduler
            // 
            this.bt_RunScheduler.Location = new System.Drawing.Point(611, 16);
            this.bt_RunScheduler.Name = "bt_RunScheduler";
            this.bt_RunScheduler.Size = new System.Drawing.Size(140, 23);
            this.bt_RunScheduler.TabIndex = 50;
            this.bt_RunScheduler.Text = "Run Scheduler";
            this.bt_RunScheduler.UseVisualStyleBackColor = true;
            this.bt_RunScheduler.Click += new System.EventHandler(this.bt_RunScheduler_Click);
            // 
            // bt_Exportini
            // 
            this.bt_Exportini.Location = new System.Drawing.Point(615, 449);
            this.bt_Exportini.Name = "bt_Exportini";
            this.bt_Exportini.Size = new System.Drawing.Size(75, 23);
            this.bt_Exportini.TabIndex = 52;
            this.bt_Exportini.Text = "Save";
            this.bt_Exportini.UseVisualStyleBackColor = true;
            this.bt_Exportini.Click += new System.EventHandler(this.bt_Exportini_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.ForeColor = System.Drawing.Color.Gray;
            this.label9.Location = new System.Drawing.Point(802, 446);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 26);
            this.label9.TabIndex = 54;
            this.label9.Text = "If you have changed,\r\n please save the data again.";
            // 
            // pBar1
            // 
            this.pBar1.Location = new System.Drawing.Point(696, 449);
            this.pBar1.Name = "pBar1";
            this.pBar1.Size = new System.Drawing.Size(100, 23);
            this.pBar1.TabIndex = 60;
            // 
            // bt_savelog
            // 
            this.bt_savelog.Location = new System.Drawing.Point(609, 7);
            this.bt_savelog.Name = "bt_savelog";
            this.bt_savelog.Size = new System.Drawing.Size(75, 23);
            this.bt_savelog.TabIndex = 61;
            this.bt_savelog.Text = "Savelog";
            this.bt_savelog.UseVisualStyleBackColor = true;
            this.bt_savelog.Click += new System.EventHandler(this.bt_RecordExp_Click);
            // 
            // tabC_
            // 
            this.tabC_.Controls.Add(this.tabP_PushScript);
            this.tabC_.Controls.Add(this.tabP_Monitor);
            this.tabC_.Controls.Add(this.tabP_WeightTest);
            this.tabC_.Location = new System.Drawing.Point(611, 45);
            this.tabC_.Name = "tabC_";
            this.tabC_.SelectedIndex = 0;
            this.tabC_.Size = new System.Drawing.Size(698, 394);
            this.tabC_.TabIndex = 62;
            // 
            // tabP_PushScript
            // 
            this.tabP_PushScript.Controls.Add(this.dgvTemplates);
            this.tabP_PushScript.Controls.Add(this.groupBox7);
            this.tabP_PushScript.Location = new System.Drawing.Point(4, 22);
            this.tabP_PushScript.Name = "tabP_PushScript";
            this.tabP_PushScript.Padding = new System.Windows.Forms.Padding(3);
            this.tabP_PushScript.Size = new System.Drawing.Size(690, 368);
            this.tabP_PushScript.TabIndex = 0;
            this.tabP_PushScript.Text = "Script";
            this.tabP_PushScript.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Recordtype_no);
            this.groupBox7.Controls.Add(this.Recordtype_yes);
            this.groupBox7.Controls.Add(this.tb_CorrectionValue);
            this.groupBox7.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox7.Location = new System.Drawing.Point(441, 88);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(232, 50);
            this.groupBox7.TabIndex = 68;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Record weight";
            // 
            // Recordtype_no
            // 
            this.Recordtype_no.AutoSize = true;
            this.Recordtype_no.Location = new System.Drawing.Point(119, 22);
            this.Recordtype_no.Name = "Recordtype_no";
            this.Recordtype_no.Size = new System.Drawing.Size(43, 17);
            this.Recordtype_no.TabIndex = 69;
            this.Recordtype_no.Text = "No";
            this.Recordtype_no.UseVisualStyleBackColor = true;
            this.Recordtype_no.CheckedChanged += new System.EventHandler(this.Recordtype_no_CheckedChanged);
            // 
            // Recordtype_yes
            // 
            this.Recordtype_yes.AutoSize = true;
            this.Recordtype_yes.Checked = true;
            this.Recordtype_yes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Recordtype_yes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Recordtype_yes.Location = new System.Drawing.Point(30, 21);
            this.Recordtype_yes.Name = "Recordtype_yes";
            this.Recordtype_yes.Size = new System.Drawing.Size(54, 18);
            this.Recordtype_yes.TabIndex = 68;
            this.Recordtype_yes.Text = "Yes";
            this.Recordtype_yes.UseVisualStyleBackColor = true;
            this.Recordtype_yes.CheckedChanged += new System.EventHandler(this.Recordtype_yes_CheckedChanged);
            // 
            // tb_CorrectionValue
            // 
            this.tb_CorrectionValue.Location = new System.Drawing.Point(172, 19);
            this.tb_CorrectionValue.Name = "tb_CorrectionValue";
            this.tb_CorrectionValue.Size = new System.Drawing.Size(45, 23);
            this.tb_CorrectionValue.TabIndex = 67;
            this.tb_CorrectionValue.Text = "0.62";
            // 
            // tabP_Monitor
            // 
            this.tabP_Monitor.Controls.Add(this.bt_CleardgvTables);
            this.tabP_Monitor.Controls.Add(this.dgvTables);
            this.tabP_Monitor.Controls.Add(this.bt_savelog);
            this.tabP_Monitor.Location = new System.Drawing.Point(4, 22);
            this.tabP_Monitor.Name = "tabP_Monitor";
            this.tabP_Monitor.Padding = new System.Windows.Forms.Padding(3);
            this.tabP_Monitor.Size = new System.Drawing.Size(690, 368);
            this.tabP_Monitor.TabIndex = 1;
            this.tabP_Monitor.Text = "Monitor";
            this.tabP_Monitor.UseVisualStyleBackColor = true;
            // 
            // bt_CleardgvTables
            // 
            this.bt_CleardgvTables.Location = new System.Drawing.Point(6, 6);
            this.bt_CleardgvTables.Name = "bt_CleardgvTables";
            this.bt_CleardgvTables.Size = new System.Drawing.Size(75, 23);
            this.bt_CleardgvTables.TabIndex = 68;
            this.bt_CleardgvTables.Text = "Clear";
            this.bt_CleardgvTables.UseVisualStyleBackColor = true;
            this.bt_CleardgvTables.Click += new System.EventHandler(this.bt_CleardgvTables_Click);
            // 
            // dgvTables
            // 
            this.dgvTables.AllowUserToAddRows = false;
            this.dgvTables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column9,
            this.Column11,
            this.Column12,
            this.Column13});
            this.dgvTables.Location = new System.Drawing.Point(2, 36);
            this.dgvTables.Name = "dgvTables";
            this.dgvTables.RowTemplate.Height = 24;
            this.dgvTables.Size = new System.Drawing.Size(682, 326);
            this.dgvTables.TabIndex = 0;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column8.HeaderText = "Time";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.FillWeight = 10F;
            this.Column9.HeaderText = "Loop";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.FillWeight = 10F;
            this.Column11.HeaderText = "Stride";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 50;
            // 
            // Column12
            // 
            this.Column12.FillWeight = 10F;
            this.Column12.HeaderText = "Weight";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.FillWeight = 20F;
            this.Column13.HeaderText = "Note";
            this.Column13.Name = "Column13";
            this.Column13.Width = 150;
            // 
            // tabP_WeightTest
            // 
            this.tabP_WeightTest.Controls.Add(this.label28);
            this.tabP_WeightTest.Controls.Add(this.tb_freq_saveimgimg);
            this.tabP_WeightTest.Controls.Add(this.cb_wt_detectpress);
            this.tabP_WeightTest.Controls.Add(this.label25);
            this.tabP_WeightTest.Controls.Add(this.tb_freq_saveimgcsv);
            this.tabP_WeightTest.Controls.Add(this.bt_cccsetform);
            this.tabP_WeightTest.Controls.Add(this.bt_ClearWTlog);
            this.tabP_WeightTest.Controls.Add(this.bt_wt_savelog);
            this.tabP_WeightTest.Controls.Add(this.dgvTables_wt);
            this.tabP_WeightTest.Controls.Add(this.bt_wt_lock);
            this.tabP_WeightTest.Controls.Add(this.label22);
            this.tabP_WeightTest.Controls.Add(this.bt_WT_calibration);
            this.tabP_WeightTest.Controls.Add(this.label21);
            this.tabP_WeightTest.Controls.Add(this.label20);
            this.tabP_WeightTest.Controls.Add(this.tb_pushtimes);
            this.tabP_WeightTest.Controls.Add(this.tb_templeWeight);
            this.tabP_WeightTest.Controls.Add(this.label19);
            this.tabP_WeightTest.Controls.Add(this.label18);
            this.tabP_WeightTest.Controls.Add(this.label17);
            this.tabP_WeightTest.Controls.Add(this.trackBar1);
            this.tabP_WeightTest.Controls.Add(this.label16);
            this.tabP_WeightTest.Controls.Add(this.nUD_curStress);
            this.tabP_WeightTest.Controls.Add(this.tb_lenInterval);
            this.tabP_WeightTest.Controls.Add(this.bt_Weightdetect);
            this.tabP_WeightTest.Location = new System.Drawing.Point(4, 22);
            this.tabP_WeightTest.Name = "tabP_WeightTest";
            this.tabP_WeightTest.Padding = new System.Windows.Forms.Padding(3);
            this.tabP_WeightTest.Size = new System.Drawing.Size(690, 368);
            this.tabP_WeightTest.TabIndex = 2;
            this.tabP_WeightTest.Text = "WeightTest";
            this.tabP_WeightTest.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label28.ForeColor = System.Drawing.Color.Gray;
            this.label28.Location = new System.Drawing.Point(383, 330);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 16);
            this.label28.TabIndex = 80;
            this.label28.Text = "攝影存取間隔";
            // 
            // tb_freq_saveimgimg
            // 
            this.tb_freq_saveimgimg.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_freq_saveimgimg.Location = new System.Drawing.Point(450, 324);
            this.tb_freq_saveimgimg.Name = "tb_freq_saveimgimg";
            this.tb_freq_saveimgimg.Size = new System.Drawing.Size(37, 22);
            this.tb_freq_saveimgimg.TabIndex = 79;
            this.tb_freq_saveimgimg.Text = "4";
            // 
            // cb_wt_detectpress
            // 
            this.cb_wt_detectpress.AutoSize = true;
            this.cb_wt_detectpress.Location = new System.Drawing.Point(25, 12);
            this.cb_wt_detectpress.Name = "cb_wt_detectpress";
            this.cb_wt_detectpress.Size = new System.Drawing.Size(76, 16);
            this.cb_wt_detectpress.TabIndex = 78;
            this.cb_wt_detectpress.Text = "DetectPress";
            this.cb_wt_detectpress.UseVisualStyleBackColor = true;
            this.cb_wt_detectpress.CheckedChanged += new System.EventHandler(this.cb_wt_detectpress_CheckedChanged);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label25.ForeColor = System.Drawing.Color.Gray;
            this.label25.Location = new System.Drawing.Point(493, 330);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(61, 16);
            this.label25.TabIndex = 76;
            this.label25.Text = "csv 存取間隔";
            // 
            // tb_freq_saveimgcsv
            // 
            this.tb_freq_saveimgcsv.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_freq_saveimgcsv.Location = new System.Drawing.Point(560, 324);
            this.tb_freq_saveimgcsv.Name = "tb_freq_saveimgcsv";
            this.tb_freq_saveimgcsv.Size = new System.Drawing.Size(37, 22);
            this.tb_freq_saveimgcsv.TabIndex = 75;
            this.tb_freq_saveimgcsv.Text = "100";
            // 
            // bt_cccsetform
            // 
            this.bt_cccsetform.Image = ((System.Drawing.Image)(resources.GetObject("bt_cccsetform.Image")));
            this.bt_cccsetform.Location = new System.Drawing.Point(259, 316);
            this.bt_cccsetform.Name = "bt_cccsetform";
            this.bt_cccsetform.Size = new System.Drawing.Size(45, 41);
            this.bt_cccsetform.TabIndex = 74;
            this.bt_cccsetform.UseVisualStyleBackColor = true;
            this.bt_cccsetform.Click += new System.EventHandler(this.bt_cccsetform_Click);
            // 
            // bt_ClearWTlog
            // 
            this.bt_ClearWTlog.Location = new System.Drawing.Point(603, 316);
            this.bt_ClearWTlog.Name = "bt_ClearWTlog";
            this.bt_ClearWTlog.Size = new System.Drawing.Size(75, 41);
            this.bt_ClearWTlog.TabIndex = 73;
            this.bt_ClearWTlog.Text = "Clear";
            this.bt_ClearWTlog.UseVisualStyleBackColor = true;
            this.bt_ClearWTlog.Click += new System.EventHandler(this.bt_ClearWTlog_Click);
            // 
            // bt_wt_savelog
            // 
            this.bt_wt_savelog.Location = new System.Drawing.Point(178, 316);
            this.bt_wt_savelog.Name = "bt_wt_savelog";
            this.bt_wt_savelog.Size = new System.Drawing.Size(75, 41);
            this.bt_wt_savelog.TabIndex = 72;
            this.bt_wt_savelog.Text = "Savelog";
            this.bt_wt_savelog.UseVisualStyleBackColor = true;
            this.bt_wt_savelog.Click += new System.EventHandler(this.bt_wt_savelog_Click);
            // 
            // dgvTables_wt
            // 
            this.dgvTables_wt.AllowUserToAddRows = false;
            this.dgvTables_wt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTables_wt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgvTables_wt.Location = new System.Drawing.Point(178, 17);
            this.dgvTables_wt.Name = "dgvTables_wt";
            this.dgvTables_wt.RowTemplate.Height = 24;
            this.dgvTables_wt.Size = new System.Drawing.Size(500, 285);
            this.dgvTables_wt.TabIndex = 71;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Time";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.FillWeight = 10F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Loop";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 90;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.FillWeight = 10F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Weight";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 70;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.FillWeight = 20F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Note";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 50;
            // 
            // bt_wt_lock
            // 
            this.bt_wt_lock.Location = new System.Drawing.Point(97, 287);
            this.bt_wt_lock.Name = "bt_wt_lock";
            this.bt_wt_lock.Size = new System.Drawing.Size(68, 70);
            this.bt_wt_lock.TabIndex = 69;
            this.bt_wt_lock.Text = "Lock";
            this.bt_wt_lock.UseVisualStyleBackColor = true;
            this.bt_wt_lock.Click += new System.EventHandler(this.bt_wt_lock_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.LimeGreen;
            this.label22.Location = new System.Drawing.Point(26, 145);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 20);
            this.label22.TabIndex = 68;
            this.label22.Text = "0.00";
            // 
            // bt_WT_calibration
            // 
            this.bt_WT_calibration.Location = new System.Drawing.Point(97, 258);
            this.bt_WT_calibration.Name = "bt_WT_calibration";
            this.bt_WT_calibration.Size = new System.Drawing.Size(68, 23);
            this.bt_WT_calibration.TabIndex = 67;
            this.bt_WT_calibration.Text = "calibration";
            this.bt_WT_calibration.UseVisualStyleBackColor = true;
            this.bt_WT_calibration.Click += new System.EventHandler(this.bt_WT_calibration_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(32, 73);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 12);
            this.label21.TabIndex = 49;
            this.label21.Text = "Weight(kg)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(37, 103);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 12);
            this.label20.TabIndex = 66;
            this.label20.Text = "Push Time";
            // 
            // tb_pushtimes
            // 
            this.tb_pushtimes.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_pushtimes.Location = new System.Drawing.Point(97, 100);
            this.tb_pushtimes.Name = "tb_pushtimes";
            this.tb_pushtimes.Size = new System.Drawing.Size(68, 22);
            this.tb_pushtimes.TabIndex = 65;
            this.tb_pushtimes.Text = "100000";
            // 
            // tb_templeWeight
            // 
            this.tb_templeWeight.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_templeWeight.Location = new System.Drawing.Point(97, 70);
            this.tb_templeWeight.Name = "tb_templeWeight";
            this.tb_templeWeight.Size = new System.Drawing.Size(68, 22);
            this.tb_templeWeight.TabIndex = 64;
            this.tb_templeWeight.Text = "0.700";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(88, 214);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 12);
            this.label19.TabIndex = 63;
            this.label19.Text = "deep(0~50mm)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(87, 173);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 12);
            this.label18.TabIndex = 62;
            this.label18.Text = "interval(Max 1)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 339);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 61;
            this.label17.Text = "50";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(34, 174);
            this.trackBar1.Maximum = 5000;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(45, 183);
            this.trackBar1.SmallChange = 70;
            this.trackBar1.TabIndex = 60;
            this.trackBar1.TickFrequency = 500;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar1.Value = 5000;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 179);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 12);
            this.label16.TabIndex = 59;
            this.label16.Text = "0";
            // 
            // nUD_curStress
            // 
            this.nUD_curStress.DecimalPlaces = 2;
            this.nUD_curStress.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.nUD_curStress.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.nUD_curStress.Location = new System.Drawing.Point(97, 230);
            this.nUD_curStress.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            131072});
            this.nUD_curStress.Name = "nUD_curStress";
            this.nUD_curStress.Size = new System.Drawing.Size(68, 22);
            this.nUD_curStress.TabIndex = 57;
            this.nUD_curStress.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.nUD_curStress.ValueChanged += new System.EventHandler(this.nUD_curStress_ValueChanged);
            this.nUD_curStress.EnabledChanged += new System.EventHandler(this.nUD_curStress_EnabledChanged);
            // 
            // tb_lenInterval
            // 
            this.tb_lenInterval.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_lenInterval.Location = new System.Drawing.Point(97, 189);
            this.tb_lenInterval.Name = "tb_lenInterval";
            this.tb_lenInterval.Size = new System.Drawing.Size(68, 22);
            this.tb_lenInterval.TabIndex = 54;
            this.tb_lenInterval.Text = "0.1";
            this.tb_lenInterval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_lenInterval_KeyPress);
            this.tb_lenInterval.Leave += new System.EventHandler(this.tb_lenInterval_Leave);
            // 
            // bt_Weightdetect
            // 
            this.bt_Weightdetect.Location = new System.Drawing.Point(25, 34);
            this.bt_Weightdetect.Name = "bt_Weightdetect";
            this.bt_Weightdetect.Size = new System.Drawing.Size(140, 23);
            this.bt_Weightdetect.TabIndex = 53;
            this.bt_Weightdetect.Text = "Detect";
            this.bt_Weightdetect.UseVisualStyleBackColor = true;
            this.bt_Weightdetect.Click += new System.EventHandler(this.bt_Weightdetect_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label23.Location = new System.Drawing.Point(1237, 441);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 17);
            this.label23.TabIndex = 70;
            this.label23.Text = "0.00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(1033, 454);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 17);
            this.label12.TabIndex = 64;
            this.label12.Text = "0";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.ForeColor = System.Drawing.Color.Gray;
            this.label13.Location = new System.Drawing.Point(940, 454);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 21);
            this.label13.TabIndex = 65;
            this.label13.Text = "Stride Index :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Stride_timer
            // 
            this.Stride_timer.Tick += new System.EventHandler(this.Stride_timer_Tick);
            // 
            // bt_ForceQuit
            // 
            this.bt_ForceQuit.Location = new System.Drawing.Point(851, 60);
            this.bt_ForceQuit.Name = "bt_ForceQuit";
            this.bt_ForceQuit.Size = new System.Drawing.Size(75, 23);
            this.bt_ForceQuit.TabIndex = 66;
            this.bt_ForceQuit.Text = "Force Quit";
            this.bt_ForceQuit.UseVisualStyleBackColor = true;
            this.bt_ForceQuit.Click += new System.EventHandler(this.bt_ForceQuit_Click);
            // 
            // RunScheduler_timer
            // 
            this.RunScheduler_timer.Interval = 200;
            this.RunScheduler_timer.Tick += new System.EventHandler(this.RunScheduler_timer_Tick);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(1145, 454);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 17);
            this.label14.TabIndex = 69;
            this.label14.Text = "0";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.ForeColor = System.Drawing.Color.Gray;
            this.label15.Location = new System.Drawing.Point(1055, 454);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 21);
            this.label15.TabIndex = 70;
            this.label15.Text = "force numbers :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackerbar_time
            // 
            this.trackerbar_time.Tick += new System.EventHandler(this.trackerbar_time_Tick);
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label24.ForeColor = System.Drawing.Color.Gray;
            this.label24.Location = new System.Drawing.Point(1179, 440);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 21);
            this.label24.TabIndex = 71;
            this.label24.Text = "weight :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // time_cur_weight
            // 
            this.time_cur_weight.Interval = 1000;
            this.time_cur_weight.Tick += new System.EventHandler(this.time_cur_weight_Tick);
            // 
            // trace_report_time
            // 
            this.trace_report_time.Tick += new System.EventHandler(this.trace_report_time_Tick);
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label26.ForeColor = System.Drawing.Color.Gray;
            this.label26.Location = new System.Drawing.Point(1179, 457);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(48, 21);
            this.label26.TabIndex = 73;
            this.label26.Text = "location :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.Color.SeaGreen;
            this.label27.Location = new System.Drawing.Point(1237, 458);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 17);
            this.label27.TabIndex = 72;
            this.label27.Text = "0.00";
            // 
            // idle_time
            // 
            this.idle_time.Interval = 2000;
            this.idle_time.Tick += new System.EventHandler(this.idle_time_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1312, 478);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tabC_);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.bt_ForceQuit);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pBar1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.bt_Exportini);
            this.Controls.Add(this.bt_RunScheduler);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.rtxtDataArea);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox_port);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_set);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button_SendCommand);
            this.Controls.Add(this.textBox3);
            this.Name = "Form1";
            this.Text = "ToyoServoCylinder Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemplates)).EndInit();
            this.tabC_.ResumeLayout(false);
            this.tabP_PushScript.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabP_Monitor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTables)).EndInit();
            this.tabP_WeightTest.ResumeLayout(false);
            this.tabP_WeightTest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTables_wt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_curStress)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_distance;
        private System.Windows.Forms.TextBox textBox_Speed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_delaytime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_port;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_com;
        private System.Windows.Forms.Button button_buttonOpenCloseCom;
        private System.Windows.Forms.Label label_COMconnect;
        private System.Windows.Forms.Button button_SearchCOM;
        private System.Windows.Forms.Button button_SendCommand;
        private System.Windows.Forms.Button button_SearchCOM2;
        private System.Windows.Forms.Label label_COMconnect2;
        private System.Windows.Forms.Button button_buttonOpenCloseCom2;
        private System.Windows.Forms.ComboBox comboBox_com2;
        private System.Windows.Forms.RichTextBox rtxtDataArea;
        private System.Windows.Forms.TextBox textBox_time;
        private System.Windows.Forms.Label label_count;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_emergencystop;
        private System.Windows.Forms.Button button_set;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Timer timer_TimeDetect;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Button button_ZeroDistance;
        private System.Windows.Forms.TextBox textBox_weight;
        private System.Windows.Forms.Button button_autostart;
        private System.Windows.Forms.Button button_Unlock;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_calibration;
        private System.Windows.Forms.Button button_Normaltest;
        private System.Windows.Forms.TextBox textBox_startdistance;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_enddistance;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        public System.Windows.Forms.DataGridView dgvTemplates;
        private System.Windows.Forms.Button bt_RunScheduler;
        public System.Windows.Forms.Button bt_Exportini;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ListBox listBox1;
        public System.Windows.Forms.ProgressBar pBar1;
        public System.Windows.Forms.Button bt_savelog;
        private System.Windows.Forms.TabControl tabC_;
        private System.Windows.Forms.TabPage tabP_PushScript;
        private System.Windows.Forms.TabPage tabP_Monitor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Timer Stride_timer;
        private System.Windows.Forms.Button bt_ForceQuit;
        private System.Windows.Forms.TextBox tb_CorrectionValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridView dgvTables;
        private System.Windows.Forms.Button ToyoOnOff;
        private System.Windows.Forms.Button bt_CleardgvTables;
        private System.Windows.Forms.Timer RunScheduler_timer;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox Recordtype_no;
        private System.Windows.Forms.CheckBox Recordtype_yes;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboBox_com3;
        private System.Windows.Forms.Button button_buttonOpenCloseCom3;
        private System.Windows.Forms.Label label_COMconnect3;
        private System.Windows.Forms.Button button_SearchCOM3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.TabPage tabP_WeightTest;
        private System.Windows.Forms.TextBox tb_lenInterval;
        public System.Windows.Forms.Button bt_Weightdetect;
        private System.Windows.Forms.NumericUpDown nUD_curStress;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tb_pushtimes;
        private System.Windows.Forms.TextBox tb_templeWeight;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button bt_WT_calibration;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button bt_wt_lock;
        private System.Windows.Forms.Timer trackerbar_time;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button bt_ClearWTlog;
        public System.Windows.Forms.Button bt_wt_savelog;
        private System.Windows.Forms.DataGridView dgvTables_wt;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Timer time_cur_weight;
        private System.Windows.Forms.Timer trace_report_time;
        private System.Windows.Forms.ListBox listBox3;
        public System.Windows.Forms.Button bt_cccsetform;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tb_freq_saveimgcsv;
        private System.Windows.Forms.CheckBox cb_wt_detectpress;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tb_freq_saveimgimg;
        private System.Windows.Forms.Timer idle_time;
    }
}

