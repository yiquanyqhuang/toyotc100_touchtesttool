﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
namespace ToyoServoCylinderTool
{
    //
    [Serializable]
    public class AddScript
    {
        private Form1 mainform = null;

        public void AddObjectToForm(Form1 form1s,string path)
        {
            mainform = form1s;
            var dgv = mainform.dgvTemplates;
            IniFile iniManager = new IniFile(path);

            for (int i = 0; i < 60; i++)
            {
                //operation_mode
                //moving_coordinate
                //moving_speed
                //wait_time
                //force
                //beused
                string idstr = (i + 1).ToString();

                string operation_mode = iniManager.ReadIniFile(idstr, "operation_mode", "default");
                if (operation_mode == "1")
                    operation_mode = "abs";

                dgv.Rows.Add(idstr,
                operation_mode,
                iniManager.ReadIniFile(idstr, "moving_coordinate", "default"),
                iniManager.ReadIniFile(idstr, "moving_speed", "default"),
                iniManager.ReadIniFile(idstr, "wait_time", "default"),
                iniManager.ReadIniFile(idstr, "force", "default"),
                iniManager.ReadIniFile(idstr, "beused", "default"),
                iniManager.ReadIniFile(idstr, "beusedRecord", "default")
                );
            }
            //debug

            //for (int i = 1; i <= 61; i++)
            //{
            //    dgv.Rows.Add(i, "abs", "24.28", 50, 1000, 0, "-1");
            //}
        }

        public void ImpostiniFile(string path)
        {
            var pBar1 = mainform.pBar1;
            // 顯示進度條控制元件.
            pBar1.Visible = true;
            // 設定進度條最小值.
            pBar1.Minimum = 1;
            // 設定進度條最大值.
            pBar1.Maximum = 60;
            // 設定進度條初始值
            pBar1.Value = 1;
            // 設定每次增加的步長
            pBar1.Step = 1;
            var dgv = mainform.dgvTemplates;
            IniFile iniManager = new IniFile(path);
            Graphics g = this.mainform.pBar1.CreateGraphics();

            for (int i = 0; i < dgv.Rows.Count ; i++)
            {
                //operation_mode
                //moving_coordinate
                //moving_speed
                //wait_time
                //force
                //beused
                string idstr = (i + 1).ToString();
                string operation_mode = dgv.Rows[i].Cells[1].Value.ToString();
                if (operation_mode == "abs")
                    operation_mode = "1";

                iniManager.WriteIniFile(idstr, "operation_mode", operation_mode);
                iniManager.WriteIniFile(idstr, "moving_coordinate", dgv.Rows[i].Cells[2].Value.ToString());
                iniManager.WriteIniFile(idstr, "moving_speed", dgv.Rows[i].Cells[3].Value.ToString());
                iniManager.WriteIniFile(idstr, "wait_time", dgv.Rows[i].Cells[4].Value.ToString());
                iniManager.WriteIniFile(idstr, "force", dgv.Rows[i].Cells[5].Value.ToString());
                iniManager.WriteIniFile(idstr, "beused", dgv.Rows[i].Cells[6].Value.ToString());
                iniManager.WriteIniFile(idstr, "beusedRecord", dgv.Rows[i].Cells[7].Value.ToString());
                pBar1.PerformStep();
                //string str = Math.Round((100 * i / 127.0), 2).ToString("#0.00 ") + "%";
                //Font font = new Font("Times New Roman", (float)10, FontStyle.Regular);
                //PointF pt = new PointF(this.mainform.pBar1.Width / 2 - 17, this.mainform.pBar1.Height / 2 - 7);
                //g.DrawString(str, font, Brushes.Blue, pt);
                //System.Threading.Thread.Sleep(60);
            }
            //pBar1.Visible = false;
            pBar1.Value = 1;
            //string str = Math.Round((100 * i / 127.0), 2).ToString("#0.00 ") + "%";
            Font font = new Font("Times New Roman", (float)10, FontStyle.Regular);
            PointF pt = new PointF(this.mainform.pBar1.Width / 2 - 25, this.mainform.pBar1.Height / 2 - 7);
            g.DrawString("Success!", font, Brushes.Red, pt);
            System.Threading.Thread.Sleep(1000);

        }

        public class IniFile
        {
            private string filePath;
            private StringBuilder lpReturnedString;
            private int bufferSize;

            [DllImport("kernel32")]
            private static extern long WritePrivateProfileString(string section, string key, string lpString, string lpFileName);

            [DllImport("kernel32")]
            private static extern int GetPrivateProfileString(string section, string key, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

            public IniFile(string iniPath)
            {
                filePath = iniPath;
                bufferSize = 512;
                lpReturnedString = new StringBuilder(bufferSize);
            }

            // read ini date depend on section and key
            public string ReadIniFile(string section, string key, string defaultValue)
            {
                lpReturnedString.Clear();
                GetPrivateProfileString(section, key, defaultValue, lpReturnedString, bufferSize, filePath);
                return lpReturnedString.ToString();
            }

            // write ini data depend on section and key
            public void WriteIniFile(string section, string key, Object value)
            {
                WritePrivateProfileString(section, key, value.ToString(), filePath);
            }


        }

    }
}
